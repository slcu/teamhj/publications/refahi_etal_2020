from common.edict import invert

def fset(it): return frozenset(it)

states = {fset(['ANT', 'AS1', 'ATML1', 'ETTIN', 'FIL', 'LFY', 'MP']) : 1,
          fset(['ANT', 'ATML1', 'ETTIN', 'LFY', 'MP', 'PHB_PHV', 'PUCHI',
                'REV']) : 2,
          fset(['ANT', 'ATML1', 'LFY', 'MP', 'PHB_PHV', 'PUCHI', 'REV']) : 3,
          fset(['ATML1', 'CUC1_2_3', 'MP', 'STM']) : 4,
          fset(['ANT', 'AP1', 'AP2', 'AS1', 'ATML1', 'ETTIN', 'FIL', 'LFY', 'MP',
                'SVP']):5,
          fset(['ANT', 'AP1', 'AP2', 'LFY', 'MP', 'PHB_PHV', 'PUCHI', 'REV',
                'STM', 'SVP']): 6,
          fset(['ANT', 'AP1', 'AP2', 'ATML1', 'LFY', 'MP', 'PHB_PHV', 'REV',
                'STM', 'SVP']): 7,
          fset(['AHP6', 'ANT', 'AP1', 'AP2', 'ATML1', 'ETTIN', 'LFY', 'MP',
                'SEP1', 'SEP2']): 8,
          fset(['ANT', 'AP1', 'AP2', 'AS1', 'ATML1', 'ETTIN', 'FIL', 'LFY', 'MP',
                'SEP1', 'SEP2', 'STM', 'SVP']): 9,
          fset(['ANT', 'AP1', 'AP2', 'ATML1', 'ETTIN', 'LFY', 'MP', 'PHB_PHV',
                'REV', 'SEP1', 'SEP2', 'STM']): 10,
          fset(['ANT', 'AP1', 'AP2', 'ATML1', 'ETTIN', 'LFY', 'MP', 'SEP1',
                'SEP2']): 11,
          fset(['ANT', 'AP1', 'AP2', 'ATML1', 'ETTIN', 'LFY', 'MP', 'SEP1',
                'SEP2', 'STM']): 12,
          fset(['AG', 'AP3', 'ATML1', 'ETTIN', 'MP', 'PHB_PHV', 'PI', 'REV',
                'SEP1', 'SEP2', 'SEP3', 'STM']): 13,
          fset(['AG', 'ATML1', 'CLV3', 'ETTIN', 'MP', 'PHB_PHV', 'REV', 'SEP1',
                'SEP2', 'SEP3', 'STM']): 14,
          fset(['AG', 'ATML1', 'ETTIN', 'MP', 'PHB_PHV', 'REV', 'SEP1', 'SEP2',
                'SEP3', 'STM']): 15,
          fset(['AHP6', 'ANT', 'AP1', 'AP2', 'AS1', 'ATML1', 'ETTIN', 'FIL', 'LFY',
                'MP', 'SEP1', 'SEP2']): 16,
          fset(['ANT', 'AP1', 'AP2', 'AS1', 'ATML1', 'ETTIN', 'FIL', 'LFY', 'MP',
                'SEP1', 'SEP2']): 17,
          fset(['ANT', 'AP1', 'AP2', 'AS1', 'ATML1', 'LFY', 'MP', 'PHB_PHV', 'REV',
                'SEP1', 'SEP2']): 18,
          fset(['AP1', 'AP2', 'AP3', 'ATML1', 'CUC1_2_3', 'MP', 'PHB_PHV', 'PI',
                'REV', 'SEP1', 'SEP2', 'STM']): 19,
          fset(['AP1', 'AP2', 'ATML1', 'CUC1_2_3', 'MP', 'SEP1', 'SEP2', 'STM']): 20,
          fset(['AP1', 'AP2', 'MP', 'SEP1', 'SEP2', 'STM']): 21,
          fset(['AG', 'ANT', 'AP3', 'ATML1', 'ETTIN', 'MP', 'PHB_PHV', 'PI', 'SEP1',
                'SEP2', 'SEP3', 'STM']): 22,
          fset(['AG', 'ANT', 'ATML1', 'ETTIN', 'MP', 'PHB_PHV', 'REV', 'SEP1', 'SEP2',
                'SEP3', 'STM']): 23,
          fset(['AG', 'ANT', 'ATML1', 'ETTIN', 'MP', 'PHB_PHV', 'REV', 'SEP1', 'SEP2',
                'SEP3', 'STM', 'SUP']): 24,
          fset(['AG', 'AP3', 'ATML1', 'CUC1_2_3', 'MP', 'PI', 'SEP1', 'SEP2', 'SEP3',
                'STM']): 25,
          fset(['AHP6', 'ANT', 'AP1', 'AP2', 'AS1', 'ATML1', 'LFY', 'MP', 'PHB_PHV',
                'REV', 'SEP1', 'SEP2']): 26,
          fset(['ANT', 'AP1', 'AP2', 'AS1', 'ATML1', 'ETTIN', 'FIL', 'LFY',
                'SEP1', 'SEP2']): 27,
          fset(['ANT', 'AP1', 'AP2', 'AS1', 'FIL', 'LFY', 'SEP1', 'SEP2']): 28,
          fset(['AP1', 'AP2', 'AP3', 'ATML1', 'CUC1_2_3', 'MP', 'PI', 'SEP1',
                'SEP2', 'STM']): 29,
          fset(['AP1', 'AP2', 'AP3', 'ATML1', 'CUC1_2_3', 'MP', 'SEP1', 'SEP2',
                'STM']): 30,
          fset(['ANT', 'AS1', 'ATML1', 'ETTIN', 'FIL', 'LFY', 'MP', 'PUCHI']): 31}

def statesI():
    return invert(states)

def statesI_env(geneNms):
    from collections import defaultdict
    
    sts = statesI()
    sts_env = defaultdict(dict)
    for st, st_gns in sts.items():
        for gn in geneNms:
            if gn in st_gns:
                sts_env[st][gn] = True
            else:
                sts_env[st][gn] = False

    return sts_env

def states_per_t():
    return {10: [4, 31, 1, 2, 3],
            40: [4, 5, 7],
            96: [12, 9, 11, 8, 10],
            120: [20, 17, 18, 16, 19, 13, 15, 14],
            132: [30, 18, 27, 26, 16, 29, 25, 22, 24, 23, 14]}

def state_genes_at(t):
    sts_t = states_per_t()
    sts = set(sts_t[t])

    return dict([(k, v) for k, v in states.items()
                 if v in sts])

def stateAsBoolF(sid):
    import ast
    ss = statesI(states)

    return 'and'.join(["'{g}`".format(g=g) for g in ss.get(sid, fset([]))])
                    
def get_state(gs):
    return states.get(gs, -1)

def get_state_at(t, gs):
    sts_t = state_genes_at(t)
    return sts_t.get(gs, None)
