from os.path import join
import os
from common.seg import (Tissue,
                        STissue,
                        Cell,
                        Vec3,
                        readDataT1,
                        readGeomT1,
                        readGeomIm,
                        readRess,
                        geneNms,
                        readDataT1Im)
import common.seg as seg
from itertools import repeat
import common.L1L2_cells_ids as layers
import common.statesN as f  # type:ignore
from functools import reduce
from operator import add
from typing import List, Tuple, Dict, Callable, TypeVar


Dt = Tuple[int, int]
Lin = Dict[int, List[int]]


def getSurvivingBetween(tss: Dict[int, Tissue],
                        linss: Dict[Dt, Lin], t1:
                        int, t2: int) -> List[int]:
    linsL = filterLinssBetween(linss, (t1, t2))

    cids = list()

    for c in tss[t1]:
        if tSearchN(linsL, c.cid, len(linsL), 0):
            cids.append(c.cid)

    return cids


def filterSurvivingUntil(tss: Dict[int, Tissue],
                         linss: Dict[Dt, Lin],
                         t2: int) -> None:
    for t, ts in tss.items():
        cids = getSurvivingBetween(tss, linss, t, t2)
        filterT(ts, cids)

    return


T = TypeVar("T")
U = TypeVar("U")
def fst(x: Tuple[T, U]) -> T: return x[0]


def snd(x: Tuple[T, U]) -> U: return x[1]


def filterT(ts: Tissue, cids: List[int]) -> None:
    scids = set(cids)
    ts.cells = dict([(c.cid, c) for c in ts if c.cid in scids])

    neighsL1 = None

    if ts.neighs:
        neighsL1 = {}
        for cid, ds in ts.neighs.items():
            if cid in scids:
                neighsL1[cid] = dict([(n, d) for n, d in ds.items() if n in scids])

    ts.neighs = neighsL1


def filterL1(tss: Dict[int, Tissue]) -> None:
    L1_cids = {10: layers.L1_10h,
               40: layers.L1_40h,
               96: layers.L1_96h,
               120: layers.L1_120h,
               132: layers.L1_132h}

    for t, ts in tss.items():
        filterT(ts, L1_cids[t])

    return


def filterL1Neigh(tss: Dict[int, Tissue]) -> Dict[int, Tissue]:
    return {t: ts.filterL1() for t, ts in tss.items()}


def filterL1_marker(tss: Dict[int, STissue]) -> None:
    def getTopExpr(e):
        return e.body[0].value

    for t, ts in tss.items():
        atml1_cids = ts.filterGBExpr(seg.ge("'ATML1'"))
        filterT(ts, atml1_cids)

    return


def filterL1_st(tss: Dict[int, STissue]) -> None:
    sts_ts = f.states_per_t()
    for t, ts in tss.items():
        cids = [c.cid for c in ts if f.get_state_at(t, c.getOnGenes())]
        filterT(ts, cids)


def filterL1L2(tss: Dict[int, Tissue]) -> None:
    L1_cids = {10: layers.L1_10h,
               40: layers.L1_40h,
               96: layers.L1_96h,
               120: layers.L1_120h,
               132: layers.L1_132h}

    L2_cids = {10: layers.L2_10h,
               40: layers.L2_40h,
               96: layers.L2_96h,
               120: layers.L2_120h,
               132: layers.L2_132h}

    for t, ts in tss.items():
        filterT(ts, L1_cids[t] + L2_cids[t])


def getTs(linFn: str) -> Tuple[int, int]:
    fn, _ = os.path.splitext(linFn)
    (_, t1, _, t2) = fn.split("_")

    return (int(t1[:-1]), int(t2[:-1]))


def getTs1(linFn: str) -> Tuple[int, ...]:
    fn, _ = os.path.splitext(linFn)

    return tuple([int(st.replace("hrs", ""))
                  for st in fn.split("_to_")])


def readLin1(linFn: str) -> Tuple[Lin, Tuple[int, ...]]:
    from os.path import split
    path, fn = split(linFn)
    lin = dict()
    with open(linFn, 'r') as fin:
        for ln in fin:
            cid_, ds_ = ln.strip().split(":")
            cid = int(cid_.strip())
            ds = [int(d.strip()) for d in ds_.strip().split(",")]
            lin[cid] = ds

    return lin, getTs1(fn)


def readLins1(linDataLoc: str) -> List[Tuple[Lin,
                                             Tuple[int, ...]]]:
    lins = [readLin1(join(linDataLoc, f)) for f in os.listdir(linDataLoc)
            if os.path.isfile(join(linDataLoc, f))]

    return lins


def mkLinsSeries1(lins):
    linss = list()
    for lin, ts in lins:
        linss.append((ts, lin))

    return sorted(linss, key=lambda lt: lt[0][0])


def after(t, lt) -> bool:
    ts, _ = lt
    (t1, t2) = ts

    return t >= t1


def before(t, lt) -> bool:
    ts, _ = lt
    (t1, t2) = ts

    return t <= t2


def between(linSeries, t1: int, t2: int) -> List[Tuple[Tuple[int, int],
                                                       Dict[int, List[int]]]]:
    afters = [i for i, lt in enumerate(linSeries) if after(t1, lt)]
    befores = [i for i, lt in enumerate(linSeries) if before(t2, lt)]

    s = afters[-1]
    e = befores[0]

    return [(ts, lin) for ts, lin in linSeries[s:(e+1)]]


def mergeLins(lin1: Dict[int, List[int]],
              lin2: Dict[int, List[int]]) -> Dict[int, List[int]]:
    mLin: Dict[int, List[int]] = dict()

    for m, ds in lin1.items():
        mLin[m] = sum([lin2.get(d, []) for d in ds], [])

    return mLin


def mergeLinss1(lins: List[Dict[int, List[int]]],
                acc: Dict[int, List[int]]) -> Dict[int, List[int]]:
    if not lins:
        return acc

    return mergeLinss1(lins[1:], mergeLins(acc, lins[0]))


def mergeLinss(lins: List[Dict[int, List[int]]]) -> Dict[int, List[int]]:
    df: Dict[int, List[int]] = {}
    if len(lins) > 2:
        return mergeLinss1(lins[2:], mergeLins(lins[0], lins[1]))
    elif len(lins) == 2:
        return mergeLins(lins[0], lins[1])
    elif len(lins) == 1:
        return lins[0]
    else:
        return df


######################################################
def concat(xss: List[List[T]]) -> List[T]:
    return sum(xss, [])


def tSearchN(mss: List[Dict[int, List[int]]],
             r: int, n: int, c: int) -> List[int]:
    if c == n:
        return [r]
    else:
        return concat([tSearchN(mss, d, n, c+1) for d in mss[c].get(r, [])])


def tSearch1(mss: List[Dict[int, List[int]]], r: int) -> List[int]:
    try:
        return mss[0][r]
    except KeyError:
        return []


def tSearch2(mss: List[Dict[int, List[int]]], r: int) -> List[int]:
    try:
        return sum([mss[1][d] for d in mss[0][r]], [])
    except KeyError:
        print(r)
        return []


def cvol(c: Cell) -> float:
    if c:
        return c.vol
    else:
        return 0.0


def sumVol(cs: List[Cell], f: Callable[[Cell], float] = cvol) -> float:
    return sum([cvol(c) for c in cs])


def mergeCellVolsRefN(tss: List[Tissue],
                      mss: List[Lin]) -> List[Dict[int, float]]:
    volss: List[Dict[int, float]] = list()

    for i, ts in enumerate(tss):
        volss.append(dict())
        for c in tss[0]:
            volss[i][c.cid] = sum(cvol(ts.cells.get(ci, None))
                                  for ci in tSearchN(mss, c.cid, i, 0))

    return volss


def const1(c: T) -> int:
    if c:
        return 1
    else:
        return 0


def mergeCellNsRefN(tss: List[Tissue],
                    linss: List[Lin]) -> List[Dict[int, int]]:
    attrss: List[Dict[int, int]] = list()

    for i, ts in enumerate(tss):
        attrss.append(dict())
        for c in tss[0]:
            attrss[i][c.cid] = sum(const1(ts.cells.get(ci, None))
                                   for ci in tSearchN(linss, c.cid, i, 0))

    return attrss


def divOr0(n1: float, n2: float) -> float:
    if n2 == 0:
        return 0.0
    else:
        return n1 / n2


def mkSeries1(d: str,
              dExprs: str,
              linDataLoc: str,
              ft=lambda x: True) -> Tuple[Dict[int, STissue], Dict[Dt, Lin]]:
    lins = mkLinsSeries1(readLins1(linDataLoc))
    all_timepoints = list(set(reduce(add, [ts for ts, lin in lins])))
    timepoints = [t for t in sorted(all_timepoints) if ft(t)]
    tss = dict()
    linss = dict()

    for t1, t2 in zip(timepoints, timepoints[1:]):
        tss[t1] = readDataT1(d, dExprs, t1)
        tss[t2] = readDataT1(d, dExprs, t2)

        linss[(t1, t2)] = mergeLinss(list(map(lambda x: x[1],
                                              between(lins, t1, t2))))

    return tss, linss


def mkSeriesGeom1(d: str,
                  linDataLoc: str,
                  ft=lambda x: True) -> Tuple[Dict[int, Tissue], Dict[Dt, Lin]]:
    lins = mkLinsSeries1(readLins1(linDataLoc))
    all_timepoints = list(set(reduce(add, [ts for ts, lin in lins])))
    timepoints = [t for t in sorted(all_timepoints) if ft(t)]
    tss = dict()
    linss = dict()

    for t1, t2 in zip(timepoints, timepoints[1:]):
        tss[t1] = readGeomT1(d, t1)
        tss[t2] = readGeomT1(d, t2)

        linss[(t1, t2)] = mergeLinss(list(map(lambda x: x[1],
                                              between(lins, t1, t2))))

    return tss, linss


def mkSeriesIm0(dataDir: str,
                fid: int = 1,
                ft: Callable[[int], bool] = lambda x: True):
    d = join(dataDir, "FM{fid}".format(fid=fid))
    dExpr = join(d, "geneExpression")
    linDataLoc = join(dataDir, "FM{fid}/tracking_data".format(fid=str(fid)))
    ressFn = join(dataDir, "FM{fid}/resolutions.txt".format(fid=str(fid)))

    ress = readRess(ressFn.format(fid=str(fid)))
    tpoints = [t for t in sorted(ress.keys()) if ft(t)]

    lins = mkLinsSeries1(readLins1(linDataLoc))
    tss = dict()
    linss = dict()

    for t1, t2 in zip(tpoints, tpoints[1:]):
        print(t1, t2)
        tss[t1] = readDataT1Im(d, dExpr, t1)
        tss[t2] = readDataT1Im(d, dExpr, t2)

        linss[(t1, t2)] = mergeLinss(list(map(lambda x: x[1],
                                              between(lins, t1, t2))))

    return tss, linss


def mkSeriesGeomIm(dataDir: str,
                   fid: int = 1,
                   ft: Callable[[int], bool] = lambda x: True):
    d = join(dataDir, "FM{fid}".format(fid=fid))
    linDataLoc = join(dataDir, "FM{fid}/tracking_data".format(fid=str(fid)))
    ressFn = join(dataDir, "FM{fid}/resolutions.txt".format(fid=str(fid)))

    ress = readRess(ressFn.format(fid=str(fid)))
    tpoints = [t for t in sorted(ress.keys()) if ft(t)]

    lins = mkLinsSeries1(readLins1(linDataLoc))
    tss = dict()
    linss = dict()

    for t in tpoints:
        print(t)
        tss[t] = readGeomIm(d, t)

    for t1, t2 in zip(tpoints, tpoints[1:]):
        print(t1, t2)
        linss[(t1, t2)] = mergeLinss(list(map(lambda x: x[1],
                                              between(lins, t1, t2))))

    return tss, linss


def writeSeriesGeomTV(tss: Dict[int, Tissue], d: str):
    for t, ts in tss.items():
        seg.writeTV(ts, d, "{t}h".format(t=str(t)))


###################################
def invertMap(ln):
    iLin = [list(zip(ds, repeat(m, len(ds)))) for m, ds in ln.items()]

    return dict(sum(iLin, []))


def updateCells(ts, ts1, iLn):
    for c1 in ts1:
        try:
            c = ts.cells[iLn[c1.cid]]
        except KeyError:
            print(c1.cid)
            c = Cell(0, Vec3(0, 0, 0), 0, dict([(g, False) for g in geneNms]))
        c1.exprs = dict([(g, c.exprs[g]) for g, v in c1.exprs.items()])

    return


def prev(t):
    if t == 40: return 10
    elif t == 96: return 40
    elif t == 120: return 96
    elif t == 132: return 120
    else: return None


def succ(t):
    if t==10: return 40
    elif t==40: return 96
    elif t==96: return 120
    elif t==120: return 132
    else: return None


def updateTs(tss, linss, tpoints):
    # evolve patterns based on lineage
    tssLin = {}
    for t in tpoints[1:]:
        tssLin[(prev(t), t)] = tss[t].updateExprsTs(tss[prev(t)],
                                                    invertMap(linss[(prev(t), t)]))

    return tssLin


def updateTsBF(tss, fs):
    return dict((t, tss[t].updateExprsBF(fs[t])) for t in tss.keys())


def tFn(t, ref=""):
    return "ts_t" + str(t) + ref + ".txt"


def betweenTs(t, bounds):
    ts, te = bounds
    return t >= ts and t <= te


def filterLinssBetween(linss, bounds):
    t1, t2 = bounds
    linssL = sorted([(ts, ln) for ts, ln in linss.items()
                     if (betweenTs(ts[0], (t1, t2)) and
                         betweenTs(ts[1], (t1, t2)))],
                    key=lambda p: p[0][0])

    return list(map(snd, linssL))


def filterTssBetween(tss, tbounds):
    t1, t2 = tbounds

    tssL = sorted([(t, ts) for t, ts in tss.items()
                   if betweenTs(t, (t1, t2))], key=fst)

    return list(map(snd, tssL))
