import numpy as np  # type: ignore
import ast  # type: ignore
from tifffile import TiffFile  # type: ignore
from copy import deepcopy  # type: ignore
from typing import Tuple, Dict, TypeVar, Callable, List, Set, Iterator
from numpy import ndarray
from os.path import join


T = TypeVar('T')
U = TypeVar('U')


BoolF = Callable[[bool, bool], bool]
GFunc = Tuple[Tuple[str, ...],
              Tuple[str, ...],
              str,
              Tuple[BoolF, ...],
              Tuple[BoolF, ...]]
GeneNm = str

geneNms: List[str] = ['AG',
                      'AHP6',
                      'ANT',
                      'AP1',
                      'AP2',
                      'AP3',
                      'AS1',
                      'ATML1',
                      'CLV3',
                      'CUC1_2_3',
                      'ETTIN',
                      'FIL',
                      'LFY',
                      'MP',
                      'PHB_PHV',
                      'PI',
                      'PUCHI',
                      'REV',
                      'SEP1',
                      'SEP2',
                      'SEP3',
                      'STM',
                      'SUP',
                      'SVP',
                      'WUS']


class Vec3():
    def __init__(self, x: float, y: float, z: float) -> None:
        self.x = x
        self.y = y
        self.z = z

    def dist(self, v1) -> float:
        return np.sqrt((self.x - v1.x)**2 +
                       (self.y - v1.y)**2 +
                       (self.z - v1.z)**2)

    def toOrganism(self, sep: str = ' ') -> str:
        return sep.join([str(self.x), str(self.y), str(self.z)])

    def swapZX(self) -> None:
        xt = self.x
        self.x = self.z
        self.z = xt

    def toArray(self):
        return np.array([self.x, self.y, self.z])

    def toTV(self) -> str:
        return repr([self.x, self.y, self.z]).replace(",", "")

    def __repr__(self) -> str:
        return self.toOrganism()


class Cell():
    def __init__(self,
                 cid: int,
                 pos: Vec3,
                 vol: float,
                 exprs: Dict[GeneNm, bool] = {}):
        self.cid = cid
        self.pos = pos
        self.vol = vol
        self.exprs = exprs

    def dist(self, c1) -> float:
        return self.pos.dist(c1.pos)

    def addExprs(self, exprs: Dict[str, bool], geneNms: List[str]):
        for gn in geneNms:
            if gn not in exprs.keys():
                exprs[gn] = False

        self.exprs = exprs
        self.geneNms = geneNms

        return

    def updateExprs(self, gfs: Dict[GeneNm, GFunc]):
        gexprsUpd = dict()

        for g, _ in self.exprs.items():
            if g in gfs:
                gf = gfs[g]
                (acts, reprs, out, fsAct, fsRepr) = gf
                inputs = acts + reprs
                vals = [self.exprs[i] for i in inputs]
                gexprsUpd[g] = evalI((acts, reprs, out), vals, fsAct, fsRepr)
            else:
                gexprsUpd[g] = self.exprs[g]

        ncell = Cell(self.cid, self.pos, self.vol)
        ncell.addExprs(gexprsUpd, deepcopy(self.geneNms))

        return ncell

    def filterGs(self, e):
        return evalB(e.body[0].value, self.exprs)

    def updateExprsC(self, c):
        exprsUpd = dict([(g, c.exprs[g]) for g, v in self.exprs.items()])
        nc = Cell(self.cid, self.pos, self.vol, exprsUpd)
        nc.geneNms = self.geneNms
        return nc

    def _toInt(self, b):
        if b:
            return 1
        else:
            return 0

    def _exprsToOrg(self, sep: str = ' ') -> str:
        exprs = [str(float(self.exprs[gn])) for gn in self.geneNms]

        return sep.join(exprs)

    def swapZX(self) -> None:
        self.pos.swapZX()

    @classmethod
    def fromTV(cls, nelems: Dict[str, str]):
        def parsePos(spos):
            pos = list(np.fromstring(spos.replace('[', '').replace(']', ''),
                                     sep=' '))
            return pos

        cid = int(nelems['cid'])
        vol = float(nelems['volume'])
        [xc, yc, zc] = parsePos(nelems['center'])
        pos = Vec3(xc, yc, zc)

        return cls(cid, pos, vol)

    @classmethod
    def fromImage(cls, cd, res: List[float]):
        volVoxel = np.prod(res)
        xc, yc, zc = cd.centroid
        nVoxels = cd.area

        xc, yc, zc = np.multiply(np.array([zc, yc, xc]), res)

        vol = nVoxels * volVoxel
        pos = Vec3(xc, yc, zc)
        cid = cd.label

        return cls(cid, pos, vol)

    def toOrganism(self) -> str:
        if self.exprs:
            return (' '.join([self.pos.toOrganism(), str(self.vol),
                              str(self.cid), self._exprsToOrg()]))
        else:
            return (' '.join([self.pos.toOrganism(), str(self.vol),
                              str(self.cid)]))

    def toText(self, sep: str = " ") -> str:
        if self.exprs:
            return (sep.join([str(self.cid),
                              self.pos.toOrganism(sep=sep),
                              str(self.vol),
                              self._exprsToOrg(sep=sep)]))
        else:
            return sep.join([str(self.cid), self.pos.toOrganism(sep=sep)])

    def centreToArray(self):
        return self.pos.toArray()

    def getOnGenes(self):
        return (frozenset([g for g, st in self.exprs.items() if st])
                - frozenset(['WUS']))

    def areExpressed(self, gs: List[str]) -> bool:
        gexpr = set([g for g, st in self.exprs.items() if st])
        return gexpr == set(gs)

    def getGeneStatus(self, geneNm: str) -> int:
        def boolToInt(b):
            if b:
                return 1
            else:
                return 0

        return boolToInt(self.exprs.get(geneNm, False))

    def toTV(self) -> str:
        pos_s = self.pos.toTV()
        cellTV = "cid: {cid}, volume: {vol}, center: {pos}"

        return cellTV.format(cid=str(self.cid),
                             vol=str(self.vol),
                             pos=pos_s)

    def __repr__(self) -> str:
        return self.toOrganism()


class Tissue():
    def __init__(self, cells: Dict[int, Cell],
                 neighs: Dict[int, Dict[int, float]],
                 ecellIds: Set[int] = set()):
        self.cells = cells
        self.neighs = neighs
        self.L1_cells = ecellIds

    def __iter__(self) -> Iterator[Cell]:
        # iterate over a list instead of map
        return iter([v for _, v in self.cells.items()])

    @classmethod
    def _extractCells(cls, data, res: List[float]) -> Dict[int, Cell]:
        from skimage.measure import regionprops  # type: ignore
        cellsSeg = regionprops(data)

        cells = dict()
        for cd in cellsSeg:
            cells[int(cd.label)] = Cell.fromImage(cd, res)

        return cells

    @classmethod
    def _getConn(cls, cells, data):
        from skimage.future.graph import RAG  # type: ignore

        connG = RAG(data)
        neigh = dict()

        for n in connG.nodes:
            nbrs = connG.adj[n].keys()
            ds = [cells[n].dist(cells[nb]) for nb in nbrs]

            neigh[n] = dict(zip(nbrs, ds))

        return neigh

    @classmethod
    def fromTV(cls, fnGeom: str, fnNeigh: str):
        def parseLn(ln: str) -> Dict[str, str]:
            ln = str.strip(ln)
            nelems = dict()
            elems = str.split(ln, ',')
            for elm in elems:
                [name, val] = str.split(elm, ':')
                name = str.strip(name)
                val = str.strip(val)
                nelems[name] = val

            return nelems

        cells = dict()
        neighs = dict()
        ecellIds = []

        with open(fnGeom, 'r') as f:
            for ln in f:
                nelems = parseLn(ln)
                c = Cell.fromTV(nelems)
                cells[c.cid] = c

        with open(fnNeigh, 'r') as f:
            for ln in f:
                nelems_ = ast.literal_eval(ln)
                cid = nelems_['cid']
                ns = nelems_['neighbors ids']

                if 1 in ns:
                    ecellIds.append(cid)
                    ns.remove(1)

                neighs[cid] = dict((n, cells[cid].dist(cells[n])) for n in ns)

        return cls(cells, neighs, set(ecellIds))

    @classmethod
    def fromTVGeom(cls, fnGeom: str):
        def parseLn(ln: str) -> Dict[str, str]:
            ln = str.strip(ln)
            nelems = dict()
            elems = str.split(ln, ',')
            for elm in elems:
                [name, val] = str.split(elm, ':')
                name = str.strip(name)
                val = str.strip(val)
                nelems[name] = val

            return nelems

        cells = dict()
        neighs = dict()
        ecellIds = []

        with open(fnGeom, 'r') as f:
            for ln in f:
                nelems = parseLn(ln)
                c = Cell.fromTV(nelems)
                cells[c.cid] = c

        return cls(cells, None, set(ecellIds))

    @classmethod
    def fromImage(cls, data, res: List[float]):
        cells = cls._extractCells(data, res)
        #conn = cls._getConn(cells, data)

        return cls(cells, None)

    def adjView(self):
        return dict([(cid, ds.keys()) for cid, ds in self.neighs.items()])

    def relabelFromMap(self, relabelM: Dict[int, int]) -> None:
        neighsR = []

        for c in self:
            c.cid = relabelM[c.cid]

        for cid, dists in self.neighs.items():
            reNs = dict([(relabelM[n], d) for n, d in dists.items()])
            neighsR.append((relabelM[cid], reNs))

        self.neighs = dict(neighsR)

        return

    def relabel(self) -> None:
        relabelM = dict([(c.cid, i) for i, c in enumerate(self)])

        self.relabelFromMap(relabelM)

        return

    def filterL1(self):
        def filter_incl(xs, xs1):
            fxs = set(xs1)

            return [x for x in xs if x in fxs]

        if not self.L1_cells:
            return

        L1_cells_ = dict([(c.cid, c) for c in self if c.cid in self.L1_cells])
        L1_neighs = None
        if self.neighs:
            L1_neighs = dict([(cid, filter_incl(nbrs, self.L1_cells))
                              for cid, nbrs in deepcopy(list(self.neighs.items()))
                              if cid in self.L1_cells])

        return Tissue(L1_cells_, L1_neighs)

    def toNeigh(self) -> str:

        def cellToNeigh(cid: int, nbs: Dict[int, float]) -> str:
            nbsIds = nbs.keys()
            nNbs = len(nbsIds)
            nbsDists = [nbs[nid] for nid in nbsIds]

            return " ".join([str(cid), str(nNbs)]
                            + list(map(str, nbsIds))
                            + list(map(str, nbsDists)))

        ncells = sum(1 for _ in self)
        header = str(ncells) + " " + "1"

        cellReprs = ([cellToNeigh(cid, nbs)
                      for cid, nbs in sorted(self.neighs.items(),
                                             key=lambda x: x[0])])

        return "\n".join([header] + cellReprs)

    def toOrganism(self) -> str:
        headerComment = "#x y z vol id"
        ns = str(len(self.cells)) + " 5"
        content = '\n'.join([cell.toOrganism() for cell in self])

        return "\n".join([headerComment, ns, content])

    def toTVGeom(self) -> str:
        return "\n".join([c.toTV() for c in self if c.cid != 1])

    def toTVNeigh(self) -> str:
        def toTVNeighCell(cid, neighs):
            nids = [nid for nid, _ in neighs.items()]
            return '{{"cid": {c}, "neighbors ids": {ns} }}'.format(c=str(cid),
                                                                   ns=repr(nids))

        return "\n".join([toTVNeighCell(cid, neighs)
                          for cid, neighs in self.neighs.items() if cid != 1])

    def toTV(self) -> Tuple[str, str]:
        return (self.toTVGeom(), self.toTVNeigh())

    def centresToArray(self):
        return np.array([c.centreToArray() for c in self])

    def getGeneStatus(geneNm, self) -> List[Tuple[int, int]]:
        return [(c.cid, c.getGeneStatus(geneNm)) for c in self]


class STissue(Tissue):
    def __init__(self,
                 cells: Dict[int, Cell],
                 neighs: Dict[int, Dict[int, float]],
                 gexprs: Dict[int, Dict[GeneNm, bool]] = dict(),
                 geneNms: List[str] = list()):
        super(STissue, self).__init__(cells, neighs)
        if gexprs and geneNms:
            self.addExprs(gexprs, geneNms)

        if not gexprs and geneNms:
            self.geneNms = geneNms
            for c in self:
                c.geneNms = geneNms

    def _defGeneExprs(self) -> Dict[GeneNm, bool]:
        return dict([(gn, False) for gn in self.geneNms])

    def addExprs(self, gexprs: Dict[int, Dict[str, bool]], geneNms: List[str]):
        self.geneNms = geneNms
        for cell in self:
            try:
                cell.addExprs(gexprs[cell.cid], geneNms)
            except KeyError:
                cell.addExprs(self._defGeneExprs(), geneNms)

        return

    def relabel(self):
        super(STissue, self).relabel()

    @classmethod
    # type: ignore
    def fromImage(cls, data, res: List[float],
                  gexprs: Dict[int, Dict[GeneNm, bool]], geneNms: List[str]):
        ts = Tissue.fromImage(data, res)
        return cls(ts.cells, ts.neighs, gexprs, geneNms)

    @classmethod
    # type: ignore
    def fromTV(cls, fnGeom: str,
               gexprs: Dict[int, Dict[GeneNm, bool]], geneNms: List[str]):
        ts = Tissue.fromTVGeom(fnGeom)
        return cls(ts.cells, None, gexprs, geneNms)

    def toOrganism(self) -> str:
        headerComment = "#x y z vol id " + ' '.join(self.geneNms)
        ns = str(len(self.cells)) + " " + str(len(self.geneNms)+5)
        content = '\n'.join([cell.toOrganism() for cell in self])

        return "\n".join([headerComment, ns, content])

    def updateExprsBF(self, gfs: Dict[GeneNm, GFunc]):
        ncells = dict([(c.cid, c.updateExprs(gfs)) for c in self])
        return STissue(ncells, deepcopy(self.neighs))

    def updateExprsTs(self, ts: Tissue, iLn: Dict[int, int]):
        ncells = {}
        for c1 in self:
            try:
                c = ts.cells[iLn[c1.cid]]
            except KeyError:
                c = Cell(0, Vec3(0, 0, 0), 0, dict([(g, False)
                                                    for g in geneNms]))

            ncells[c1.cid] = c1.updateExprsC(c)

        return STissue(ncells, deepcopy(self.neighs))

    def toDict(self) -> Dict[int, Dict[str, int]]:
        tsDict = {}

        for c in self:
            tsDict[c.cid] = dict(((g, boolToInt(v))
                                  for g, v in c.exprs.items()))

        return tsDict

    def toCSV(self) -> str:
        headerComment = "id,x,y,z,vol," + ','.join(self.geneNms)

        content = '\n'.join([cell.toText(sep=",") for cell in self])

        return "\n".join([headerComment, content])

    def filterGBExpr(self, e):
        return [c.cid for c in self if c.filterGs(e)]

    def filterGs(self, gs: List[str]) -> List[int]:
        return [c.cid for c in self if c.areExpressed(gs)]

    def filterL1(self):
        def filter_incl(xs, xs1):
            fxs = set(xs1)

            return [x for x in xs if x in fxs]

        if not self.L1_cells:
            return

        L1_cells_ = dict([(c.cid, c) for c in self if c.cid in self.L1_cells])
        L1_neighs = dict([(cid, filter_incl(nbrs, self.L1_cells))
                          for cid, nbrs in deepcopy(list(self.neighs.items()))
                          if cid in self.L1_cells])

        gexprs_ = deepcopy(self.gexprs)
        geneNms_ = [g for g in self.geneNms]

        return STissue(L1_cells_, L1_neighs, gexprs_, geneNms_)

    def addProp(self, f: Callable[[Cell], U], nm: str):
        self.geneNms = self.geneNms[::] + [nm]
        for c in self:
            c.exprs[nm] = f(cell=c)
            c.geneNms = c.geneNms[::] + [nm]


# reading
def readImage(fseg: str):
    f = TiffFile(fseg)
    seg = f.asarray().astype(int)

    return seg


def readImageRegionPoints(fseg: str,
                          res: List[float] = [1, 1, 1],
                          d: int = 3) -> Dict[int, ndarray]:
    from skimage.measure import regionprops  # type: ignore

    cell_regions = dict()

    seg = readImage(fseg)
    cellsSeg = regionprops(seg)

    for cregion in cellsSeg:
        cid = int(cregion.label)
        if cid != 1:
            coords = cregion.coords[:, [2, 1, 0]]
            coords_: ndarray = np.multiply(coords,
                                           res[:d])
            cell_regions[cid] = coords_

    return cell_regions


def mergePoints(cell_regions, cids):
    return np.vstack(tuple([cell_regions[cid] for cid in cids]))


def getCellRegions(d: str,
                   fid: int = 1,
                   ft: Callable[[int], bool] = lambda t: True,
                   dim: int = 3) -> Dict[int, Dict[int, ndarray]]:

    segFn = join(d, "FM{fid}/segmentation_tiffs/{t}h_segmented.tif")
    ressFn = join(d, "FM{fid}/resolutions.txt")
    ress = readRess(ressFn.format(fid=str(fid)))
    tpoints = [t for t in sorted(ress.keys()) if ft(t)]

    fsegs = dict([(t, segFn.format(fid=fid, t=str(t))) for t in tpoints])

    return dict([(t, readImageRegionPoints(fn, res=ress[t], d=dim))
                 for t, fn in fsegs.items()])


def readExprs(fn: str) -> Dict[int, Dict[str, bool]]:
    gexprs = dict()

    with open(fn) as f:
        geneNms = f.readline().rstrip().split(' ')[1:]

        for line in f:
            els = line.rstrip().split(' ')
            cid = els[0]
            exprs = els[1:]
            gexprs[int(cid)] = dict(zip(geneNms, [bool(int(exp))
                                                  for exp in exprs]))

        return gexprs


def writeOrg(fnGeom, fnNeigh, ts):
    with open(fnGeom, 'w+') as f:
        f.write(ts.toOrganism())

    with open(fnNeigh, 'w+') as f:
        f.write(ts.toNeigh())

    return


def writeTV(ts, d, lab):

    fnGeom = "{lab}_segmented_tvformat_volume_position.txt".format(lab=lab)
    fnNeigh = "{lab}_segmented_tvformat_neighbors.txt".format(lab=lab)

    geomRepr, neighRepr = ts.toTV()

    with open(join(d, fnGeom), "w") as fout:
        fout.write(geomRepr)

    with open(join(d, fnNeigh), "w") as fout:
        fout.write(neighRepr)


def readRess(fn: str) -> Dict[int, List[float]]:
    import ast

    with open(fn, 'r') as f:
        sress = ("{" + f.read().replace("=", ":").replace("h", "").
                 replace("\n", "").replace("t", "") + "}")
        ress = ast.literal_eval(sress)

    return ress


# functions for common tasks
def mkOrgs(d, dExprs, dOut):
    from os.path import join

    timepoints = [10, 40, 96, 120, 132]
    geomF = "_segmented_tvformat_0_volume_position.txt"
    neighF = "_segmented_tvformat_0_neighbors_py.txt"
    exprF = "h.txt"

    initF = ".init"
    orgNeighF = ".neigh"

    for t in timepoints:
        print(t)
        fnGeom = join(d, "t" + str(t) + geomF)
        fnNeigh = join(d, "t" + str(t) + neighF)
        fnExpr = join(dExprs, "t_" + str(t) + exprF)
        fnGeomOut = join(dOut, "t" + str(t) + initF)
        fnNeighOut = join(dOut, "t" + str(t) + orgNeighF)

        gexprs = readExprs(fnExpr)
        ts = STissue.fromTV(fnGeom, fnNeigh, gexprs, geneNms)

        writeOrg(fnGeomOut, fnNeighOut, ts)

    return


def mkOrgGeomState(fim, fseg, fexpr, fout):
    (data, res) = readImage(fim, fseg)
    gexprs = readExprs(fexpr)

    ts = STissue.fromImage(data, res, gexprs, geneNms)

    writeOrg(fout, ts)


def mkOrgGeom(fim, fseg, fout):
    (data, res) = readImage(fim, fseg)
    ts = Tissue.fromImage(data, res)

    writeOrg(fout, ts)


def ge(s: str):
    return ast.parse(s)


def evalOp(e, env):
    if type(e.op) is ast.And:
        return all([evalB(e1, env) for e1 in e.values])
    elif type(e.op) is ast.Or:
        return any([evalB(e1, env) for e1 in e.values])
    elif type(e.op) is ast.Not:
        return not(evalB(e.operand, env))


def evalB(e, env):
    if type(e) is ast.UnaryOp or type(e) is ast.BoolOp:
        return evalOp(e, env)
    elif type(e) is ast.Constant:
        return env[e.value]
    elif type(e) is ast.Str:
        return env[e.s]


def boolToInt(b: bool) -> int:
    if b:
        return 1
    else:
        return 0


def boolsToInt(bs: List[bool]) -> List[int]:
    return [boolToInt(b) for b in bs]


def const2(a: T, b: U) -> U: return b


def folds1(vals: List[T],
           fs: Tuple[Callable[[U, T], U], ...],
           iVal: U) -> U:
    acc = iVal
    for i, v in enumerate(vals):
        acc = fs[i](acc, v)

    return acc


def folds(vals: List[bool], fs: Tuple[BoolF, ...]) -> bool:
    ff1: Tuple[BoolF, ...] = (const2, )
    fs = ff1 + fs

    return folds1(vals, fs, False)


def evalI(intr,
          vals: List[bool],
          fsAct: Tuple[BoolF, ...],
          fsRepr: Tuple[BoolF, ...]) -> bool:
    acts = intr[0]
    reprs = intr[1]

    nActs = len(acts)
    nReprs = len(reprs)

    if vals[nActs:]:
        return (folds(vals[:nActs], fsAct) and
                not folds(vals[nActs:], fsRepr))
    else:
        return folds(vals[:nActs], fsAct)


def readDataT1(d: str, dExprs: str, t: int) -> STissue:
    from os.path import join

    geomF = "{t}h_segmented_tvformat_volume_position.txt".format(t=str(t))
    exprF = "h.txt"

    fnGeom = join(d, geomF)
    fnExpr = join(dExprs, "t_" + str(t) + exprF)

    gexprs = readExprs(fnExpr)
    ts = STissue.fromTV(fnGeom, gexprs, geneNms)

    return ts


def readGeomT1(d: str, t: int) -> Tissue:
    from os.path import join

    geomF = "{t}h_segmented_tvformat_volume_position.txt".format(t=str(t))
    neighF = "{t}h_segmented_tvformat_neighbors.txt".format(t=str(t))

    fnGeom = join(d, geomF)
    fnNeigh = join(d, neighF)

    ts = Tissue.fromTV(fnGeom, fnNeigh)

    return ts


def readDataT1Im(d: str, dExprs: str, t: int) -> STissue:
    from os.path import join

    exprF = "h.txt"
    fnRes = join(d, "resolutions.txt")
    fnSeg = join(d, "segmentation_tiffs/{t}h_segmented.tif".format(t=str(t)))
    fnExpr = join(dExprs, "t_" + str(t) + exprF)

    data = readImage(fnSeg)
    ress = readRess(fnRes)
    res = ress[t]

    gexprs = readExprs(fnExpr)
    ts = STissue.fromImage(data, res, gexprs, geneNms)

    return ts


def readGeomIm(d: str, t: int) -> Tissue:
    from os.path import join

    fnRes = join(d, "resolutions.txt")
    fnSeg = join(d, "segmentation_tiffs/{t}h_segmented.tif".format(t=str(t)))

    data = readImage(fnSeg)
    ress = readRess(fnRes)
    res = ress[t]

    ts = Tissue.fromImage(data, res)

    return ts


def cellsToDat(fn, cells):
    # so we can use newman
    nvars = len(cells[0].exprs.keys()) + 5
    ncells = len(cells)
    ntpoints = 1

    header = "\n".join([str(ntpoints),
                        " ".join([str(ncells), str(nvars), "0"])])

    with open(fn, 'w+') as fout:
        fout.write("\n".join([header] + [cell.toOrganism() for cell in cells]))

    return
