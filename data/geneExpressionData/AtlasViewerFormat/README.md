## Gene expression data in AtlasViewer format

Each folder contains gene expression data in AtlasViewer format for a specific 
layer (L1, L2, L1&L2) and all cells.

To visualise patterns, first run AtlasViewer via atlasviewer/launch_aviewer.py. 
Drag and drop the segmentation file and the visualization widget and then drag 
and drop the pattern file. Select the genes to visualize form the right pane and
click "Atlas gene expression" button from the AtlasViewer menu.

To read the .pat files in the folders:

import cPickle

 fobj = file("patternFileName.pat")
 
 patterns = cPickle.load(fobj)
 
 fobj.close()
 
 
 #patterns is a Python dictionary whose keys are genes and values are cell 
 labels expressing the gene.