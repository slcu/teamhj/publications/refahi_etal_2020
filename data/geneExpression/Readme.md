# Directory for gene expression data

Two types of files are provided, generated from Morphonet and via script `mnet.py`.

## 1) Files for individual genes

These files are named GENE.txt where GENE is the gene name. Each line of the file
holds three values '<i>timeIndex, cellIndex: expression</i>'. 

<i>timeIndex (0-17)</i> is an index for time point and relates to times used for 
populating the atlas with gene expression data as follows:

```
timeIndex 1 <-> time t=10h
timeIndex 5 <-> time t=40h
timeIndex 12 <-> time t=96h
timeIndex 15 <-> time t=120h
timeIndex 17 <-> time t=132h
```

<i>cellIndex</i> is the index of the cell coming from segmentation (tissueviewer).
Note, these indices do not have to be consecutive and start >2 (where 1 in a 
segmentation relates to the background compartment).

<i>expression</i> represents boolean expression of the gene in the cell at the time point
where 2 is off and 3 is on.

These files are an output from the Morphonet software.

## 2) Files for individual time points

These files, named 't_Th.txt', where T is the time point (10,40,96,120,132).
The files have the first row:
```
CID GENE1 GENE2 ... GENE_N
```
where CID is for cellID and GENE_i is a list of gene names, followed by a matrix
with 1/0 numbers for expression of the gene in cell cellId:
```
cellId1 1 0 ... 1
...
cellIdN 0 1 ... 0
```
Note that in these files only genes expressed in at least one cell are listed.

These files are generated by running script `mnet.py`.
