NB. Make sure that the paths in fileNames.py are correct.


Description of files:
=====================

patterns_genes.py: generate a binary map of patterns and genes

reorderHammingDistances.py : reorder hamming distances table according to clustering indexes and plots the hamming distance table with new indexes. If a the patterns order has change mannualy first set mannualSet to True and then set the mannual ordering to newPatternsOrder.

hierarchicalClustering.py: Hierarchical clustering of cell states using Hamming distances as the measure of similarity.

plotBackwardTransitionGraphs.py: first set the threshold used to prune the transition edges, i.e. "threshold" at the beginning of script. Save the forward transition graphs in p,g files

plotBackwardTransitionGraphs.py: first set the threshold used to prune the transition edges, i.e. "threshold" at the beginning of script. Save the backward transition graphs in p,g files

