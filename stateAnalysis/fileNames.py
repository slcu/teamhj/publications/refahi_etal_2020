
__doc__ = """
Given the path of the directory which contains the files, it generates dictionaries whose keys are time points and values are paths
"""

# YR_01_path = "/media/yassin/HD/Data/Yassin/atlas/YR_01/"
YR_01_path = "/home/yassin/teamYRProjects/publications/atlas/data/"

YR_01_path_timepoints = [0 ,  10 ,  18 ,  24 ,  32 ,  40 ,  48 ,  57 ,  64 ,  72 ,  81 ,  88 ,  96 ,  104 ,  112 ,  120 ,  128 ,  132]# ,  137]


YR_01_segmentations_file_names = dict(("%dh"%tp, YR_01_path + "segmentations/" + "t%d_segmented.tif"%tp) for tp in YR_01_path_timepoints)

YR_01_tracking_file_names = {( str(YR_01_path_timepoints[i]) + "h", str(YR_01_path_timepoints[i + 1]) + "h" ): YR_01_path + "tracking/matchingScores_%dh_to_%dh.lin"%(YR_01_path_timepoints[i], YR_01_path_timepoints[i + 1]) for i in xrange(len(YR_01_path_timepoints) - 1)}


atlas_timePoints = [10, 40, 96, 120, 132]


atlas_pattern_fileNames_L1 = dict(("%dh"%tp, YR_01_path + "patterns/" + "patterns_%dh_L1.pat"%tp) for tp in atlas_timePoints)
atlas_pattern_fileNames_L2 = dict(("%dh"%tp, YR_01_path + "patterns/" + "patterns_%dh_L2.pat"%tp) for tp in atlas_timePoints)
atlas_pattern_fileNames_L1L2 = dict(("%dh"%tp, YR_01_path + "patterns/" + "patterns_%dh_L1_L2.pat"%tp) for tp in atlas_timePoints)
atlas_pattern_fileNames_all = dict(("%dh"%tp, YR_01_path + "patterns/" + "patterns_%dh.pat"%tp) for tp in atlas_timePoints)



if __name__ == "__main__":
    print atlas_pattern_fileNames_all
    print YR_01_segmentations_file_names
    print YR_01_tracking_file_names





