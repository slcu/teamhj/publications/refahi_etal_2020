import numpy as np
import cPickle
from scipy.spatial.distance import hamming, pdist
from scipy.cluster.hierarchy import fcluster
import matplotlib.pyplot as plt
from scipy.cluster.hierarchy import dendrogram, linkage, cophenet
import scipy.cluster.hierarchy as hac

from allTagsDModule import allTagsD_L1_new
from computeHammingDistances_L1 import dist


dist = dist[1:, 1:] # because pattern 0 does not exist

         
counter = 0
cidIndex = dict()
indexCid = dict()


Z = linkage(dist, 'ward')


c, coph_dists = cophenet(Z, pdist(dist))

p1 = plt.figure(figsize=(25, 10))

plt.title('Hierarchical Clustering Dendrogram')
plt.xlabel('sample index')
plt.ylabel('distance')
dendrogram(
    Z,
    leaf_rotation=90.,  # rotates the x axis labels
    leaf_font_size=12.,  # font size for the x axis labels
)
pos, ticks = plt.xticks()
newTicks = []
for t in ticks:
    newTicks.append(int(t.get_text()) + 1)
plt.xticks(pos, newTicks)
print "newPatternsOrder = ", newTicks
reorderedPatterns = [int(r) for r in newTicks]
    

if __name__ == "__main__":
    plt.show()






    