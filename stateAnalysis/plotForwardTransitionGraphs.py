import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
import pylab
import cPickle
from allTagsDModule import allTagsD_L1_new, allTagsD_L2_new

threshold = 0.2
from fileNames import YR_01_path

from computeHammingDistances_L1 import dist as hammingD
from computeForwardTransitionGraph import dist_10h_40h_L1, dist_40h_96h_L1, dist_96h_120h_L1, dist_120h_132h_L1
from computeForwardTransitionGraph import dist_10h_40h_L2, dist_40h_96h_L2, dist_96h_120h_L2, dist_120h_132h_L2

allTagsDreverseL1 = dict((v, k) for k, v in allTagsD_L1_new.iteritems())
allTagsDreverseL2 = dict((v, k) for k, v in allTagsD_L2_new.iteritems())

fobj = file("FM1_dtissue.tis")
dtis = cPickle.load(fobj)
fobj.close()

wwDes = []

fName = "transitionGraph10h_40h_L1"

A = np.matrix(dist_10h_40h_L1)
A[A < threshold] = 0
GMain = nx.from_numpy_matrix(A, create_using=nx.MultiDiGraph())

counter = 0
for e in GMain.edges():
    counter += 1

G = nx.DiGraph()
D = nx.get_edge_attributes(GMain,'weight')
for item, w in D.iteritems():
    w = hammingD[item[0], item[1]]
    w *= 27
    wDes = dist_10h_40h_L1[item[0], item[1]]
    wDes = np.round(wDes, 2)
    G.add_edges_from([(item[0], item[1])], label = str(int(w)) + ", " + str(wDes))
    wwDes.append([w, wDes])
    
p = nx.drawing.nx_pydot.to_pydot(G)
p.write_png('%s_threshold_%0.2f.png'%(fName, threshold))


#========================================================================================================

wwDes = []

fName = "transitionGraph40h_96h_L1"

A = np.matrix(dist_40h_96h_L1)
A[A < threshold] = 0
 
GMain = nx.from_numpy_matrix(A, create_using=nx.MultiDiGraph())
    
counter = 0
for e in GMain.edges():
    counter += 1

G = nx.DiGraph()
D = nx.get_edge_attributes(GMain,'weight')
for item, w in D.iteritems():
    w = hammingD[item[0], item[1]]
    w *= 27
    wDes = dist_40h_96h_L1[item[0], item[1]]
    wDes = np.round(wDes, 2)
    G.add_edges_from([(item[0], item[1])], label = str(int(w)) + ", " + str(wDes))
    wwDes.append([w, wDes])
    
p = nx.drawing.nx_pydot.to_pydot(G)
p.write_png('%s_threshold_%0.2f.png'%(fName, threshold))

# print wwDes

#========================================================================================================

wwDes = []

fName = "transitionGraph96h_120h_L1"

A = np.matrix(dist_96h_120h_L1)
A[A < threshold] = 0
 
GMain = nx.from_numpy_matrix(A, create_using=nx.MultiDiGraph())
    
counter = 0
for e in GMain.edges():
    counter += 1

G = nx.DiGraph()
D = nx.get_edge_attributes(GMain,'weight')
for item, w in D.iteritems():
    w = hammingD[item[0], item[1]]
    w *= 27
    wDes = dist_96h_120h_L1[item[0], item[1]]
    wDes = np.round(wDes, 2)
    G.add_edges_from([(item[0], item[1])], label = str(int(w)) + ", " + str(wDes))
    wwDes.append([w, wDes])
    
p = nx.drawing.nx_pydot.to_pydot(G)
p.write_png('%s_threshold_%0.2f.png'%(fName, threshold))


#========================================================================================================

wwDes = []

fName = "transitionGraph120h_132h_L1"

A = np.matrix(dist_120h_132h_L1)
A[A < threshold] = 0
 
GMain = nx.from_numpy_matrix(A, create_using=nx.MultiDiGraph())
counter = 0
for e in GMain.edges():
    counter += 1

G = nx.DiGraph()
D = nx.get_edge_attributes(GMain,'weight')
for item, w in D.iteritems():
    w = hammingD[item[0], item[1]]
    w *= 27
    wDes = dist_120h_132h_L1[item[0], item[1]]
    wDes = np.round(wDes, 2)
    G.add_edges_from([(item[0], item[1])], label = str(int(w)) + ", " + str(wDes))
    wwDes.append([w, wDes])
    
p = nx.drawing.nx_pydot.to_pydot(G)
p.write_png('%s_threshold_%0.2f.png'%(fName, threshold))




#========================================================================================================

wwDes = []

fName = "transitionGraph10h_40h_L2"
A = np.matrix(dist_10h_40h_L2)
A[A < threshold] = 0
 
GMain = nx.from_numpy_matrix(A, create_using=nx.MultiDiGraph())
    
counter = 0
for e in GMain.edges():
    counter += 1

G = nx.DiGraph()
D = nx.get_edge_attributes(GMain,'weight')
for item, w in D.iteritems():
    w = hammingD[item[0], item[1]]
    w *= 27
    wDes = dist_10h_40h_L2[item[0], item[1]]
    wDes = np.round(wDes, 2)
    G.add_edges_from([(item[0], item[1])], label = str(int(w)) + ", " + str(wDes))
    wwDes.append([w, wDes])
    
p = nx.drawing.nx_pydot.to_pydot(G)
p.write_png('%s_threshold_%0.2f.png'%(fName, threshold))


#========================================================================================================

wwDes = []

fName = "transitionGraph40h_96h_L2"

A = np.matrix(dist_40h_96h_L2)
A[A < threshold] = 0
 
GMain = nx.from_numpy_matrix(A, create_using=nx.MultiDiGraph())
    
counter = 0
for e in GMain.edges():
    counter += 1

G = nx.DiGraph()
D = nx.get_edge_attributes(GMain,'weight')
for item, w in D.iteritems():
    w = hammingD[item[0], item[1]]
    w *= 27
    wDes = dist_40h_96h_L2[item[0], item[1]]
    wDes = np.round(wDes, 2)
    G.add_edges_from([(item[0], item[1])], label = str(int(w)) + ", " + str(wDes))
    wwDes.append([w, wDes])
    
p = nx.drawing.nx_pydot.to_pydot(G)
p.write_png('%s_threshold_%0.2f.png'%(fName, threshold))


#========================================================================================================

wwDes = []

fName = "transitionGraph96h_120h_L2"
A = np.matrix(dist_96h_120h_L2)
A[A < threshold] = 0
 
GMain = nx.from_numpy_matrix(A, create_using=nx.MultiDiGraph())
counter = 0
for e in GMain.edges():
    counter += 1

G = nx.DiGraph()
D = nx.get_edge_attributes(GMain,'weight')
for item, w in D.iteritems():
    w = hammingD[item[0], item[1]]
    w *= 27
    wDes = dist_96h_120h_L2[item[0], item[1]]
    wDes = np.round(wDes, 2)
    G.add_edges_from([(item[0], item[1])], label = str(int(w)) + ", " + str(wDes))
    wwDes.append([w, wDes])
    
p = nx.drawing.nx_pydot.to_pydot(G)
p.write_png('%s_threshold_%0.2f.png'%(fName, threshold))


#========================================================================================================

wwDes = []

fName = "transitionGraph120h_132h_L2"

A = np.matrix(dist_120h_132h_L2)
A[A < threshold] = 0
 
GMain = nx.from_numpy_matrix(A, create_using=nx.MultiDiGraph())
    
counter = 0
for e in GMain.edges():
    counter += 1

G = nx.DiGraph()
D = nx.get_edge_attributes(GMain,'weight')
for item, w in D.iteritems():
    w = hammingD[item[0], item[1]]
    w *= 27
    wDes = dist_120h_132h_L2[item[0], item[1]]
    wDes = np.round(wDes, 2)
    G.add_edges_from([(item[0], item[1])], label = str(int(w)) + ", " + str(wDes))
    wwDes.append([w, wDes])
    
p = nx.drawing.nx_pydot.to_pydot(G)
p.write_png('%s_threshold_%0.2f.png'%(fName, threshold))






