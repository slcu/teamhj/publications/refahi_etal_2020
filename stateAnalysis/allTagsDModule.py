import cPickle

from createTagsD import allTagsD_L1, allTagsD_L2, allTagsD_L1_without_ATML1

__doc__ = """
dictionaries from createTagsD.py for L1 and L2 are combined into single dictionaries for L1 and L2: allTagsD_L1_new, allTagsD_L2_new
only these dictionaries should be used.
"""


patternsL1WithoutATML1 = set(allTagsD_L1_without_ATML1.keys())  

patternsL2 = set(allTagsD_L2.keys())


patternsL2subtractL1 =  patternsL2 - patternsL1WithoutATML1

patternsL1subtractL2 = patternsL1WithoutATML1 - patternsL2



allTagsD_L1_L2 = dict(allTagsD_L2)

nextCode = max(allTagsD_L1_L2.values()) + 1

for pat in patternsL1subtractL2:
    allTagsD_L1_L2[pat] = nextCode
    nextCode += 1
     


allTagsD_L2_new = dict(allTagsD_L1_L2) 

allTagsD_L1_new = dict()

for k, v in allTagsD_L1_L2.iteritems():
    newK = set(k)
    newK.add("ATML1")
    if newK in allTagsD_L1.keys():
        allTagsD_L1_new[frozenset(newK)] = v
    else:
        allTagsD_L1_new[frozenset(k)] = v



tagsCodeTuplesL1 = [(c, t) for t, c in allTagsD_L1_new.iteritems()]

tagsCodeTuplesL1.sort()

if __name__ == "__main__":
    print "patterns numbers in L1: ", len(allTagsD_L1)
    print "patterns numbers in L2: ", len(allTagsD_L2)
    print "patterns numbers in L1, & L2: ", len(allTagsD_L1_L2), max(allTagsD_L1_L2.values())
    print "forL1: ", allTagsD_L1_new
    print "forL2: ", allTagsD_L2_new


    