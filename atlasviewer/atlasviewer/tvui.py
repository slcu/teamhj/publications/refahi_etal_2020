############################################################################
#
# File author(s): Yassin REFAHI <yassin.refahi@inrae.fr>
# Copyright (c) 2017 - 2020, Yassin Refahi
# All rights reserved.
# * Redistribution and use in source and binary forms, with or without
# * modification, are not permitted.
#
############################################################################
import sys
import numpy as np
import vtk
import sip
import tvinfo
sip.setapi('QString', 2)
from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import QApplication
from vtk.qt4.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor
from IPython.qt.console.rich_ipython_widget import RichIPythonWidget
from IPython.qt.inprocess import QtInProcessKernelManager
from icons_rc import *
import matplotlib.pyplot as plt
from IPython.core.display import display, HTML
import atlasviewer.avinfo

__version__ = atlasviewer.avinfo.__version__
__author__ = atlasviewer.avinfo.__author__
__email__ = atlasviewer.avinfo.__email__


class DragToQVTKRenderWindowInteractor(QVTKRenderWindowInteractor):
    def __init__(self, MainWindow, parent=None):
        super(DragToQVTKRenderWindowInteractor, self).__init__(parent=parent)
        self.setAcceptDrops(True)
        self.MainWindow = MainWindow
 
    def dragEnterEvent(self, event):
        event.accept()
 
    def dropEvent(self, event):
        if event.mimeData().hasUrls():
            for url in event.mimeData().urls():
                path = url.toLocalFile()
        self.MainWindow.droppedOnVTKWindow(path)
        

class Ui_MainWindow(object):
    def setupUi(self, MainWindow, imageFileName):
        self.MainWindow = MainWindow
        MainWindow.setObjectName("MainWindow")
        MainWindow.setWindowTitle("AtlasViewer: " + imageFileName.split("/")[-1])
#         MainWindow.setWindowIcon(QtGui.QIcon("/home/yassin/icon.png"))
        self.centralWidget = QtGui.QWidget(MainWindow)
        self.gridlayout = QtGui.QGridLayout(self.centralWidget)
        self.vtkWidget = DragToQVTKRenderWindowInteractor(MainWindow, 
                                                          self.centralWidget)
        self.gridlayout.addWidget(self.vtkWidget, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralWidget)
        self.wallRGBValues = [0, 0, 0]
        self.geneRGBValues = [0, 1, 0]
        self.toolbar = MainWindow.addToolBar('toolsBar')
        self.toolbar.setStyleSheet("""QToolTip { 
                           background-color: black; 
                           color: white; 
                           border: black solid 1px
                           }""")
        
        self.dockWidget1 = QtGui.QDockWidget(MainWindow)
        self.dockWidgetContents1 = QtGui.QWidget()
        self.dockWidget1.setWidget(self.dockWidgetContents1)
        self.dockWidget1.setAllowedAreas(QtCore.Qt.DockWidgetArea(
                QtCore.Qt.LeftDockWidgetArea | QtCore.Qt.LeftDockWidgetArea))
        MainWindow.addDockWidget(QtCore.Qt.LeftDockWidgetArea,self.dockWidget1)
                
        openFile = QtGui.QAction(QtGui.QIcon(':/open.png'), "open", MainWindow)
        
        
        snapshotAction = QtGui.QAction(QtGui.QIcon(':/snapshot.png'), 
                                       'snapshot', MainWindow)
        snapshotAction.setShortcut('Ctrl+S')
        self.toolbar.addAction(snapshotAction)
        snapshotAction.setStatusTip('Snapshot')
        snapshotAction.triggered.connect(lambda : MainWindow.snapshot(None))
        
        threeDAction = QtGui.QAction(QtGui.QIcon(':/threeD.png'), 
                                     '3D render', MainWindow)
        threeDAction.setShortcut('Ctrl+T')
        self.toolbar.addAction(threeDAction)
        threeDAction.setStatusTip('Render')
        threeDAction.triggered.connect(MainWindow.threeDAct)
        
        colorAction = QtGui.QAction(QtGui.QIcon(':/segment.png'), 
                                    'separate colors', MainWindow)
        colorAction.setShortcut('Ctrl+T')
        self.toolbar.addAction(colorAction)
        colorAction.setStatusTip('separate colors')
        colorAction.triggered.connect(MainWindow.changeColormap)

        atlasAction = QtGui.QAction(QtGui.QIcon(':/mesh.png'), 
                                   'Atlas gene expression', MainWindow)
        atlasAction.setShortcut('Ctrl+A')
        self.toolbar.addAction(atlasAction)
        atlasAction.setStatusTip('Atlas gene expression')
        atlasAction.triggered.connect(MainWindow.showGeneExpressionAtlas)
        
        gbox = QtGui.QGroupBox()
        gbox.setTitle("AtlasViewer Parameters")
        gbox.setFont(QtGui.QFont("Ubuntu", 10, QtGui.QFont.Normal))
        vbox1_test = QtGui.QVBoxLayout()
        vbox1_test.addWidget(gbox)
        
        vbox1_test.addItem(QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, 
                            QtGui.QSizePolicy.Expanding))
                 
        self.dockWidgetContents1.setLayout(vbox1_test)
        
        vbox1 = QtGui.QVBoxLayout(gbox)
        gbox.setLayout(vbox1)
        
        gbox_imageType = QtGui.QGroupBox()
        gbox_imageType.setTitle("Image type")
        gbox_imageType.setFont(QtGui.QFont("Ubuntu", 10, QtGui.QFont.Normal))
        
        imageTypeHBox1 = QtGui.QHBoxLayout()
        gbox_imageType.setLayout(imageTypeHBox1)
        vbox1.addWidget(gbox_imageType)
        
        self.segmentationRadioButton = QtGui.QRadioButton()
        segmentationRadioButtonLabel = QtGui.QLabel('segmented')
        imageTypeHBox1.addWidget(segmentationRadioButtonLabel)
        imageTypeHBox1.addWidget(self.segmentationRadioButton)
#         self.segmentationRadioButton.setChecked(True)
        
        self.intensityRadioButton = QtGui.QRadioButton()
        intensityRadioButtonLabel = QtGui.QLabel('intensity')
        imageTypeHBox1.addWidget(intensityRadioButtonLabel)
        imageTypeHBox1.addWidget(self.intensityRadioButton)
        
        
        vbox1.addLayout(imageTypeHBox1)
        
        gbox_cmap = QtGui.QGroupBox()
        gbox_cmap.setTitle("Colormap")
        gbox_cmap.setFont(QtGui.QFont("Ubuntu", 10, QtGui.QFont.Normal))
        
        hbox1 = QtGui.QHBoxLayout()
        gbox_cmap.setLayout(hbox1)
        vbox1.addWidget(gbox_cmap)
        
        hbox1.addStretch(1)
        self.combo=QtGui.QComboBox()
        cMaps = [cm for cm in plt.cm.datad if not cm.endswith("_r")]        
        self.combo.insertItems(1, cMaps)
        self.combo.setEditable(True)
        colorMapLabel = QtGui.QLabel('Cell')
        
        self.combo_geneCM = QtGui.QComboBox()
        self.combo_geneCM.insertItems(1, cMaps)
        self.combo_geneCM.setEditable(True)
        geneColormapLabel = QtGui.QLabel('Gene')
        
        hbox1.addWidget(colorMapLabel)
        hbox1.addWidget(self.combo)
        
        hbox1.addWidget(geneColormapLabel)
        hbox1.addWidget(self.combo_geneCM)
        
        vbox1.addLayout(hbox1)
        
        self.colorsOpacitySlider = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.colorsOpacitySlider.setTickInterval(1)
        self.colorsOpacitySlider.setMinimum(0)
        self.colorsOpacitySlider.setValue(100)
        self.colorsOpacitySlider.setMaximum(100)
        self.colorsOpacitySlider.setSingleStep(1)
        
        self.wallsOpacitySlider = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.wallsOpacitySlider.setTickInterval(1)
        self.wallsOpacitySlider.setMinimum(0)
        self.wallsOpacitySlider.setValue(0)
        self.wallsOpacitySlider.setMaximum(100)
        self.wallsOpacitySlider.setSingleStep(1)
        
        
        hbox_rendering = QtGui.QHBoxLayout()
        self.HDRendering = QtGui.QCheckBox("Texture mapping")
        self.HDRendering.setChecked(True)
        
        self.cellWallsCBox = QtGui.QCheckBox("Cell walls")
        self.cellWallsCBox.setChecked(False)

        hbox_rendering.addWidget(self.HDRendering)
        hbox_rendering.addWidget(self.cellWallsCBox)
                
        vbox1.addLayout(hbox_rendering)
        
        
#         hbox_AtlasGlobalCode = QtGui.QHBoxLayout()
#         self.AtlasGlobalCode = QtGui.QCheckBox("Global pattern codes")
#         self.AtlasGlobalCode.setChecked(True)
#         self.AtlasGlobalCode.setStatusTip("Use Atlas global patterns code")
#         
# 
#         hbox_AtlasGlobalCode.addWidget(self.AtlasGlobalCode)
                
#         vbox1.addLayout(hbox_AtlasGlobalCode)
        
        
        
        vbox_opacity = QtGui.QVBoxLayout()
        
        gbox_opacity = QtGui.QGroupBox()
        gbox_opacity.setTitle("opacity")
        gbox_opacity.setFont(QtGui.QFont("Ubuntu", 10, QtGui.QFont.Normal))
        
        hbox_opacity = QtGui.QHBoxLayout()
        gbox_opacity.setLayout(vbox_opacity)
        vbox1.addWidget(gbox_opacity)
        
        colorsOpacityLabel = QtGui.QLabel('cells')
        hbox_opacity.addWidget(colorsOpacityLabel)
        hbox_opacity.addWidget(self.colorsOpacitySlider)
        vbox_opacity.addLayout(hbox_opacity)
        
        wallsOpacityLabel = QtGui.QLabel('walls')
        hbox_wopacity = QtGui.QHBoxLayout()
        hbox_wopacity.addWidget(wallsOpacityLabel)
        hbox_wopacity.addWidget(self.wallsOpacitySlider)
        vbox_opacity.addLayout(hbox_wopacity)
        
        vbox1.addLayout(vbox_opacity)
        
        hbox_colorButton = QtGui.QHBoxLayout()
        self.colorBtn = QtGui.QPushButton('Wall color')
        self.colorBtn.clicked.connect(self.wallColorDialog)
        hbox_colorButton.addWidget(self.colorBtn)        
        vbox1.addLayout(hbox_colorButton)
        
        gbox_dataSpacing = QtGui.QGroupBox()
        gbox_dataSpacing.setTitle("Data spacing")
        gbox_dataSpacing.setFont(QtGui.QFont("Ubuntu", 10, 
                                        QtGui.QFont.Normal))
        vbox1.addWidget(gbox_dataSpacing)
        
        hbox_dataSpacing = QtGui.QHBoxLayout()
        gbox_dataSpacing.setLayout(hbox_dataSpacing)
        hbox_x = QtGui.QHBoxLayout()
        xLabel = QtGui.QLabel('x')
        hbox_x.addWidget(xLabel)
        self.xSpinBox = QtGui.QDoubleSpinBox()
        self.xSpinBox.setDecimals(2)
        self.xSpinBox.setValue(1)
        hbox_x.addWidget(self.xSpinBox)
        hbox_x.addStretch()
        hbox_dataSpacing.addLayout(hbox_x)
        
        hbox_y = QtGui.QHBoxLayout()
        yLabel = QtGui.QLabel('y')
        hbox_y.addWidget(yLabel)
        self.ySpinBox = QtGui.QDoubleSpinBox()
        self.ySpinBox.setValue(1)
        self.ySpinBox.setDecimals(2)
        hbox_y.addWidget(self.ySpinBox)
        hbox_y.addStretch()
        hbox_dataSpacing.addLayout(hbox_y)
        
        hbox_z = QtGui.QHBoxLayout()
        zLabel = QtGui.QLabel('z')
        hbox_z.addWidget(zLabel)
        self.zSpinBox = QtGui.QDoubleSpinBox()
        self.zSpinBox.setValue(1)
        self.zSpinBox.setDecimals(2)
        hbox_z.addWidget(self.zSpinBox)
        hbox_dataSpacing.addLayout(hbox_z)
        
        vbox1.addLayout(hbox_dataSpacing)
        
        self.menubar = MainWindow.menuBar()
        self.fileMenu = self.menubar.addMenu('&File')
        self.helpMenu = self.menubar.addMenu('&Help')

        aboutAction = QtGui.QAction(QtGui.QIcon(':/aboutus.png'), '&About', 
                                    MainWindow)        
        aboutAction.setShortcut('Ctrl+A')
        aboutAction.setStatusTip('About ...')
        aboutAction.triggered.connect(self.about)
        self.helpMenu.addAction(aboutAction)
        self.stBar = MainWindow.statusBar()
        
        exitAction = QtGui.QAction(QtGui.QIcon(':/exit.png'), '&Exit', MainWindow)        
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(QtGui.qApp.quit)
        self.fileMenu.addAction(exitAction)
        self.toolbar.addAction(exitAction)
        
        self.addVolumeClippingAction = QtGui.QAction(QtGui.QIcon(":/changeStyle.png"), '&Add volume clipping plane', MainWindow)        
        self.addVolumeClippingAction.setStatusTip('Add volume clipping plane')
        
        self.addVolumeClippingAction.triggered.connect(MainWindow.addVolumeClipperPlane)
        self.fileMenu.addAction(self.addVolumeClippingAction)
        self.toolbar.addAction(self.addVolumeClippingAction)
#         self.addVolumeClippingAction.setDisabled(True)

        self.removeVolumeClippingAction = QtGui.QAction(QtGui.QIcon(":/erase.png"), '&Disable volume clipping plane', MainWindow)        
        self.removeVolumeClippingAction.setStatusTip('Remove volume clipping plane')
        
        self.removeVolumeClippingAction.triggered.connect(MainWindow.disableVolumeClipperPlane)
        self.fileMenu.addAction(self.removeVolumeClippingAction)
        self.toolbar.addAction(self.removeVolumeClippingAction)
#         self.addVolumeClippingAction.setDisabled(True)

        self.enableVolumeClippingAction = QtGui.QAction(QtGui.QIcon(":/enable.png"), '&Enable volume clipping plane', MainWindow)        
        self.enableVolumeClippingAction.setStatusTip('Enable volume clipping plane')
        
        self.enableVolumeClippingAction.triggered.connect(MainWindow.enableVolumeClipperPlane)
        self.fileMenu.addAction(self.enableVolumeClippingAction)
        self.toolbar.addAction(self.enableVolumeClippingAction)
#         self.addVolumeClippingAction.setDisabled(True)
        
        ### Integrating IPython
        self.kernel_manager = QtInProcessKernelManager()
        self.kernel_manager.start_kernel()
        self.ipython_kernel = self.kernel_manager.kernel
        self.ipython_kernel.gui = 'qt4'
        
        namespace = {"cellsToShow": [], "expressionValues" : np.array([]), 
                     "cellsToRemove": [], "cellsValues" : dict(), 
                     "cellsToMesh" : [], "geneTagsCIds": {}, 
                     "maxExpressionValue": None, "minExpressionValue": None} # self.namespace = {"cellsToShow": [], "geneExpressionValues" : np.array([0, 1, 10, 11, 120, 130]), "cellsToRemove": []}
        self.ipython_kernel.shell.push(namespace)
        self.ipython_kernel.shell.enable_pylab()
        
        self.userNamespace = self.ipython_kernel.shell.user_ns
        self.kernel_client = self.kernel_manager.client()
        self.kernel_client.start_channels()
        
        self.ipython_widget = RichIPythonWidget()
        self.ipython_widget.font_size = 20 
        self.ipython_widget.in_prompt = "tvLab >> " 
#         ipython_widget.gui_completion = "droplist"
        
        self.ipython_widget.kernel_manager = self.kernel_manager
        self.ipython_widget.kernel_client = self.kernel_client
        self.ipython_widget.exit_requested.connect(self.stopIPython)
        
        self.dockWidget2 = QtGui.QDockWidget(MainWindow)
        self.dockWidgetContents2 = QtGui.QWidget()
        self.dockWidget2.setWidget(self.dockWidgetContents2)
#         self.dockWidget2.setAllowedAreas(QtCore.Qt.DockWidgetArea(QtCore.Qt.LeftDockWidgetArea | QtCore.Qt.LeftDockWidgetArea))
        MainWindow.addDockWidget(QtCore.Qt.BottomDockWidgetArea, 
                                 self.dockWidget2)
        
        vbox2 = QtGui.QVBoxLayout(self.dockWidgetContents2)
        hbox2_1 = QtGui.QHBoxLayout()
        
#         hbox2_1.addWidget(self.pyConsole)
        
        
        self.textOutputW = QtGui.QTextEdit()
        self.textOutputW.setReadOnly(True)
#         hbox2_1.addWidget(self.textOutputW, 1)
        
#         hbox2_1.addWidget(self.ipython_widget, 2)
        
        vbox2.addLayout(hbox2_1)
        self.dockWidgetContents2.setLayout(vbox2)      
        
        self.dockWidget3 = QtGui.QDockWidget(MainWindow)
        self.dockWidgetContents3 = QtGui.QWidget()
        self.dockWidget3.setWidget(self.dockWidgetContents3)
#         self.dockWidget2.setAllowedAreas(QtCore.Qt.DockWidgetArea(QtCore.Qt.LeftDockWidgetArea | QtCore.Qt.LeftDockWidgetArea))
        MainWindow.addDockWidget(QtCore.Qt.RightDockWidgetArea, 
                                 self.dockWidget3)
        
        vbox1_test_new = QtGui.QVBoxLayout()

        gbox_new = QtGui.QGroupBox()
        gbox_new.setTitle("AtlasViewer")
        gbox_new.setFont(QtGui.QFont("Ubuntu", 10, QtGui.QFont.Normal))
        vbox1_test_new.addWidget(gbox_new)
        
        vbox1_test_new.addItem(QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, 
                            QtGui.QSizePolicy.Expanding))
                 
        self.dockWidgetContents3.setLayout(vbox1_test_new)
        
        self.vbox1_new = QtGui.QVBoxLayout(gbox_new)
        gbox_new.setLayout(self.vbox1_new)
        
        pic = QtGui.QLabel()
        pixmap = QtGui.QPixmap(":/tvlogoQuarter.png")
        pic.setPixmap(pixmap)
        vbox1_test_new.addWidget(pic)       
        
        myFont=QtGui.QFont()
        myFont.setBold(True)
        
        hbox_tree = QtGui.QHBoxLayout()
        self.tree = QtGui.QTreeWidget()
        self.tree.setHeaderLabel("Expression pattern")
        
        headerItem  = QtGui.QTreeWidgetItem()
        item = QtGui.QTreeWidgetItem()

        self.genesItem = QtGui.QTreeWidgetItem(self.tree)
        self.genesItem.setText(0, "genes")
        self.genesItem.setFlags(self.genesItem.flags() | QtCore.Qt.ItemIsTristate | QtCore.Qt.ItemIsUserCheckable)
        self.genesItem.setCheckState(0, QtCore.Qt.Unchecked)
        
        self.tree.show()
        hbox_tree.addWidget(self.tree)
        self.vbox1_new.addLayout(hbox_tree)
        
    def stopIPython(self):
            self.kernel_client.stop_channels()
            self.kernel_manager.shutdown_kernel()
            sys.exit()
    
    def wallColorDialog(self):      
        col = QtGui.QColorDialog.getColor()
        self.wallRGBValues = col.getRgb()
        self.wallRGBValues = (self.wallRGBValues[0] / 255.0, 
                              self.wallRGBValues[1] / 255.0, 
                              self.wallRGBValues[2] / 255.0, 
                              self.wallRGBValues[3]/ 255.0)
        
    def about(self):
        text = """
        <h4>AtlasViewer</h4>
        Version: %s <br>
        Author: %s (%s)
        """%(__version__, __author__, __email__)
        about = QtGui.QMessageBox.about(self.MainWindow, "AtlasViewer", text)
        
