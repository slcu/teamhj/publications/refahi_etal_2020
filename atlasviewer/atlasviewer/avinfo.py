############################################################################
#
# File author(s): Yassin REFAHI <yassin.refahi@inrae.fr>
# Copyright (c) 2017 - 2020, Yassin Refahi
# All rights reserved.
# * Redistribution and use in source and binary forms, with or without
# * modification, are not permitted.
#
############################################################################
__version__ = "0.9.5"
__author__ = "Yassin Refahi"
__email__ = "yassin.refahi@inrae.fr"
__projects__ = ["AtlasViewer"]

