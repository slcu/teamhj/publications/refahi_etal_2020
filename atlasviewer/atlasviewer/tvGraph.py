############################################################################
#
# File author(s): Yassin REFAHI <yassin.refahi@inrae.fr>
# Copyright (c) 2017 - 2020, Yassin Refahi
# All rights reserved.
# Redistribution and use in source and binary forms, with or without
# modification, are not permitted.
#
############################################################################


import vtk
import matplotlib
import matplotlib.pyplot as plt

class myActor(vtk.vtkActor):
    def setId(self, id):
        self.id = id
    def getId(self):
        return self.id
    


class Edge(object):
    def __init__(self, e, eid, property = None):
        self.source = e[0]
        self.target = e[1]
        self.eid = eid
        if property is None:
            self.property = {}
        else:
            self.property = property
    
    def __str__(self):
        return str("%d: (%s, %s)"%(self.eid, str(self.source), str(self.target)))

class Vertex(object):
    def __init__(self, k, vid, property = None):
        self.key = k
        self.vid = vid
        if property is None:
            self.property = {}
        else:
            self.property = property
    
    def __str__(self):
        return str(self.key)

class TVGraph(object):
    def __init__(self, vertices, edges, directed = False):
        self.vertices = {}
        self.edges = {}
        self.vertexNb = 0
        self.edgeNb = 0
        self.directed = directed
        for k in vertices:
            self.vertexNb += 1
            self.vertices[k] = Vertex(k, self.vertexNb)
        
        for e in edges:
            self.edgeNb += 1
            self.edges[self.edgeNb] = Edge((self.vertices[e[0]], self.vertices[e[1]]), self.edgeNb)
            
    def removeDoubleEdges(self):
        edges = set()
        self.undirectedEdges = dict()
        if not self.directed:
            for eid, e in self.edges.iteritems():
                if (e.source, e.target) not in edges:
                    edges.add((e.source, e.target))
                    edges.add((e.target, e.source))
                    self.undirectedEdges[eid] = e
                    
                    

def computeCidRGB(colorLabelValues, colorMapName = "jet"):
    normal = matplotlib.colors.Normalize(vmin=colorLabelValues[0], vmax=colorLabelValues[1])
    colMap = plt.get_cmap(colorMapName)
    sm = matplotlib.cm.ScalarMappable(norm=normal, cmap=colMap)
    cidRGB = {}            
    for i in xrange(colorLabelValues[0], colorLabelValues[1] + 1):
        cidRGB[i]= sm.to_rgba(i)
    return cidRGB


# def graghToActors(graph, vertexRadius = 1.5, edgeRaduis = 0.5, colormap = None, cmap = None):
#     if cmap is not None:
#         cidRGB = computeCidRGB(colorLabelValues = [0, 129], colorMapName = cmap)
#     
#     verticesActors = []
#     edgeActors = []
#     for vid, v in graph.vertices.iteritems():
#         if vid != 1:
#             source = vtk.vtkSphereSource()
#             source.SetCenter(v.property["center"][0], v.property["center"][1], v.property["center"][2])
#             source.SetRadius(vertexRadius)
#             source.SetPhiResolution(16)
#             source.SetThetaResolution(16)
#             mapper = vtk.vtkPolyDataMapper()
#             if vtk.VTK_MAJOR_VERSION <= 5:
#                 mapper.SetInput(source.GetOutput())
#             else:
#                 mapper.SetInputConnection(source.GetOutputPort())
#             actor = myActor()
#             actor.setId(vid)
#             if cmap is None:
#                 actor.GetProperty().SetColor((1, 0, 1))
#             else:
#                 RGB = cidRGB[colormap[vid]][:-1]
#                 actor.GetProperty().SetColor(RGB)
#             actor.SetMapper(mapper)
#             verticesActors.append(actor)
#     
#     for eid, e in graph.undirectedEdges.iteritems():
#         if (e.source.key != 1) and (e.target.key != 1):
#             c0 = e.source.property["center"]
#             c1 = e.target.property["center"]
#             Line=vtk.vtkLineSource()
#             Line.SetPoint1(c0[0], c0[1], c0[2])
#             Line.SetPoint2(c1[0], c1[1], c1[2])
#             vtkTubeFilter = vtk.vtkTubeFilter()
#             vtkTubeFilter.SetInputConnection(Line.GetOutputPort())
#             vtkTubeFilter.SetRadius(edgeRaduis)
#             vtkTubeFilter.SetNumberOfSides(10)
#             vtkTubeFilter.CappingOn()
#             vtkPolyDataMapper = vtk.vtkPolyDataMapper()
#             vtkPolyDataMapper.SetInputConnection(vtkTubeFilter.GetOutputPort())
#             vtkActor = vtk.vtkActor()
#             vtkActor.SetMapper(vtkPolyDataMapper)
#             edgeActors.append(vtkActor)
#     return verticesActors, edgeActors
#          