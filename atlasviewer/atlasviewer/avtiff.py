############################################################################
#
# File author(s): Yassin REFAHI <yassin.refahi@inrae.fr>
# Copyright (c) 2017 - 2020, Yassin Refahi
# All rights reserved.
# * Redistribution and use in source and binary forms, with or without
# * modification, are not permitted.
#
############################################################################


import numpy as np
import libtiff
from tifffile import imread as externalImread
from tifffile import imsave as externalImsave
import tifffile
import atlasviewer


def avimread(imageFileName):
    image = externalImread(imageFileName)
    tif = libtiff.TIFF.open(imageFileName, mode='r')
    XResolution = float(tif.GetField('XResolution'))
    YResolution = float(tif.GetField('YResolution'))
    description = tif.GetField("ImageDescription")
    voxelWidth = 1.0 / XResolution
    voxelHeight = 1.0 / YResolution
    descriptionItems = description.split("\n")
    voxelDepth = None
    imageType = None
    for item in descriptionItems:
        if item.startswith("spacing="):
            voxelDepth = float(item[8:])
        elif item.startswith("type="):
            imageType = str(item[5:])
    info = {"XResolution" : XResolution, "YResolution" : YResolution, "voxelWidth" : voxelWidth, "voxelHeight" : voxelHeight, "voxelDepth" : voxelDepth, "type" : imageType}
    return image, info
    

def avimsave(imageFileName, image, description = None, voxelWidth = 1, voxelHeight = 1, voxelDepth = 1, type = "intensity"):
    """
    type is either segmented or intensity
    """
    if description is None:
        description = "unit=micrometer\nspacing=%f\nsoftware=tissueviewer\ntype=%s\n"%(voxelDepth, type)
    externalImsave(imageFileName, image, description = description, resolution = (1.0 / voxelWidth, 1.0 / voxelHeight, None))
    
    
def avimsaveRGB(imageFileName, image, description = None, voxelWidth = 1, voxelHeight = 1, voxelDepth = 1, type = "intensity"):
    """
    type is either segmented or intensity
    """
    if description is None:
        description = "unit=micrometer\nspacing=%f\nsoftware=tissueviewer\ntype=%s\n"%(voxelDepth, type)
    externalImsave(imageFileName, image, description = description, resolution = (1.0 / voxelWidth, 1.0 / voxelHeight, None), photometric = "rgb" )        
        

def voxelDimensionsFromTiffFile(tifFName):
    """
    Read tif file and return the resolution 
    """
    tif = libtiff.TIFF.open(tifFName, mode='r')
    XResolution = tif.GetField('XResolution')
    YResolution = tif.GetField('YResolution')
    if XResolution != 0:
        voxelWidth = 1.0 / XResolution
    else:
        voxelWidth = None
    if YResolution != 0:
        voxelHeight = 1.0 / YResolution
    else:
        YResolution = None
    voxelDepth = None
    for item in tif.GetField("ImageDescription").split("\n"):
        if item.startswith("spacing"):
            voxelDepth = float(item[8:])
    return voxelWidth, voxelHeight, voxelDepth


def setImageType(tifFileName, imageType, newFileName = None):
    """
    imageType: segmented / intensity
    """    
    image, info = atlasviewer.avimread(tifFileName)
    if newFileName is None:
        newFileName = tifFileName[:-4] + "_tvformat.tif"
    print newFileName 
    atlasviewer.avimsave(newFileName, image, voxelWidth = info["voxelWidth"], voxelHeight = info["voxelHeight"], voxelDepth = info["voxelDepth"], type = imageType)



    

