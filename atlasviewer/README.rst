AtlasViewer Project
=======================

An environment to visualize 3D images 


Contact
=======================

Yassin Refahi (yassin.refahi@inrae.fr)

Installation
=======================

The package is developed in python (2.7), and depends on for example qt, vtk.


To install on Ubuntu (LTS 16.04):


Install dependencies:

sudo apt-get install python-setuptools

sudo apt-get install python-vtk

sudo apt-get install python-matplotlib

sudo apt-get install python-pythonlibtiff

sudo apt-get install python-scipy

sudo apt-get install python-numpy

sudo apt-get install python-sip

sudo apt-get install python-qt4

sudo apt-get install ipython

sudo apt-get install ipython-qtconsole 

sudo apt-get install python-skimage

sudo apt-get install scons

sudo apt-get install build-essential 

sudo apt-get install libz-dev



To install AtlasViewer:

python setup.py install

