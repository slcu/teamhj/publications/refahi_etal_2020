FROM python:3

RUN apt-get update && apt-get install -y python3-pip

RUN apt-get install parallel

COPY requirements.txt .

RUN pip install -r requirements.txt

# Install jupyter
RUN pip3 install ipython
RUN pip3 install notebook

# Create a new system user
RUN useradd -ms /bin/bash demo

# Change to this new user
USER demo

ADD ./common /home/common/

# Set the container working directory to the user home folder
WORKDIR /home/floweratlas4d/

RUN pip3 install /home/common/

# If you want to go directly to a shell to play with the data uncomment the
# following line
#ENTRYPOINT ["ipython"]


#docker build . -t atlas
#docker run -it --mount type=bind,source="$(pwd)",target=/home/floweratlas4d/ atlas
#import common.lin as lin
#load FM1 at specified timepoints
#tss, linss = lin.mkSeries1(d="./data/FM1/tv/",
#                           dExprs="./data/geneExpression/",
#                           linDataLoc="./data/FM1/tracking_data/",
#                           ft=lambda t: t in {10, 40, 96, 120, 132})