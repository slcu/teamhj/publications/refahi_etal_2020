from __future__ import division
from mpl_toolkits.mplot3d import axes3d
import grn
import analyseExprs as a
import matplotlib.pyplot as plt
import numpy as np
import itertools
import common.seg as seg
from os.path import join
from subprocess import call
from functools import reduce
import os
import math

geneNms = ['AG',
           'AHP6',
           'ANT',
           'AP1',
           'AP2',
           'AP3',
           'AS1',
           'CLV3',
           'CUC1_2_3',
           'ETTIN',
           'FIL',
           'LFY',
           'MP',
           'PHB_PHV',
           'PI',
           'PUCHI',
           'REV',
           'SEP3',
           'STM',
           'SUP',
           'SVP',
           'WUS']

def cellsToOrg(fn, cells):
    header = "x y z vol id " + ' '.join(cells[0].geneNms)
    with open(fn, 'w+') as fout:
        fout.write("\n".join([header] + [cell.toOrganism() for cell in cells]))

    return

def cellsToDat(fn, cells):
    #so we can use newman
    nvars = len(cells[0].exprs.keys()) + 5
    ncells = len(cells)
    ntpoints = 1

    header = "\n".join([str(ntpoints),
                        " ".join([str(ncells), str(nvars), "0"])])

    with open(fn, 'w+') as fout:
        fout.write("\n".join([header] + [cell.toOrganism() for cell in cells]))

    return

def getFsAll(intrs, cells, f):
    gintrs = dict()
    for intr in intrs:
        out = intr[2]
        print(out)
        bterms = f(intr, cells)
        gintrs[out] = bterms

    return gintrs
        
def isFunction(ios):
    domain = [i for i, o in ios]
    
    return len(domain) == len(set(domain))

def intToBool(i):
    if i == 0: return False
    else: return True

def intsToBools(ns):
    return [intToBool(n) for n in ns]

def infoP(correct, p):
    correct = set([i + (o, ) for i, o in correct])
    if tuple(intsToBools(p)) in correct: return " + "
    else: return ""

def acts(f):
    (acts, _, _, _, _) = f

    return acts

def reprs(f):
    (_, reprs, _, _, _) = f

    return reprs

def diffBF(f, f1):
    dacts = set.difference(set(acts(f1)), set(acts(f)))
    dreprs = set.difference(set(reprs(f1)), set(reprs(f)))

    dnacts = set.difference(set(acts(f)), set(acts(f1)))
    dnreprs = set.difference(set(reprs(f)), set(reprs(f1)))

    return (dacts, dreprs, set.union(dnacts, dnreprs))

def plotBoolF1(cells, f, f1, axs):
    (acts, reprs, out, fsActs, fsReprs) = f1
    (dacts, dreprs) = diffBF(f, f1)
    inputs = acts + reprs
    intr = (acts, reprs, out)
    tt = a.Interaction(inputs, out, 0.0).mkTruthTable(cells)
    correct = evalTT(tt, intr, fsActs, fsReprs)
    
    ninputs = len(inputs)
    
    ios = np.array([boolsToInt(list(p[0] + (p[1],))) for p in tt])
    ngenes = ios.shape[1]
    npats = ios.shape[0]
    
    ps = a.psGk(cells, list(inputs + (out,)))
    score = getScoreMI(tt, ps, intr, fsActs, fsReprs)
    percCorrect = getScore(tt, correct, ps) * 100
    
    xlabels = list(inputs + (out,))
    ylabels = [infoP(correct, ios[p, :]) + str(round(ps[tuple(ios[p, :])], 2))
               for p in xrange(npats)]
    
    axs.matshow(ios,cmap=plt.cm.gray_r, alpha = 0.75, aspect='auto')
    xaxis = axs.xaxis
    yaxis = axs.yaxis
    
    xlocs = np.array([i for i in xrange(ngenes)])
    ylocs = np.array([i for i in xrange(npats)])

    xaxis.set_ticks(xlocs + 0.5, minor=True)
    xaxis.set(ticks=xlocs, ticklabels=xlabels)
    
    yaxis.set_ticks(ylocs + 0.5, minor=True)
    yaxis.set(ticks=ylocs, ticklabels=ylabels)
    
    #if not isFunction(tt): axs.set_xlabeel('not a function', color='r')
    axs.set_xlabel(bfToString(f) + "\n" +
                   str(round(percCorrect, 2)) + "\n"
                   "MI=" + str(round(score, 2)) + "\n"
    )
    xaxis.grid(True, which='minor')
    yaxis.grid(True, which='minor')

    for xt in axs.get_xticklabels():
        if xt.get_text() in dacts or xt.get_text() in dreprs:
            xt.set_color('red')

    return

def plotBoolF(cells, f, axs):
    (acts, reprs, out, fsActs, fsReprs) = f
    inputs = acts + reprs
    
    ninputs = len(inputs)
    tt = a.Interaction(inputs, out, 0.0).mkTruthTable(cells)
    intr = (acts, reprs, out)
    correct = evalTT(tt, intr, fsActs, fsReprs)

    ios = np.array([boolsToInt(list(p[0] + (p[1],))) for p in tt])
    ngenes = ios.shape[1]
    npats = ios.shape[0]
    
    ps = a.psGk(cells, list(inputs + (out,)))
    score = getScoreMI(tt, ps, intr, fsActs, fsReprs)
    percCorrect = getScore(tt, correct, ps) * 100
    
    xlabels = list(inputs + (out,))
    ylabels = [infoP(correct, ios[p, :]) + str(round(ps[tuple(ios[p, :])], 2))
               for p in xrange(npats)]


    
    axs.matshow(ios,cmap=plt.cm.gray_r, alpha = 0.75, aspect='auto')
    xaxis = axs.xaxis
    yaxis = axs.yaxis
    
    xlocs = np.array([i for i in xrange(ngenes)])
    ylocs = np.array([i for i in xrange(npats)])

    xaxis.set_ticks(xlocs + 0.5, minor=True)
    xaxis.set(ticks=xlocs, ticklabels=xlabels)
    
    yaxis.set_ticks(ylocs + 0.5, minor=True)
    yaxis.set(ticks=ylocs, ticklabels=ylabels)
    
    axs.set_xlabel(bfToString(f) + "\n" +
                   str(round(percCorrect, 2)) + "\n"
                   "MI=" + str(round(score, 2)) + "\n"
                   )
    xaxis.grid(True, which='minor')
    yaxis.grid(True, which='minor')



    return

def plotBoolF(cells, f, axs):
    (acts, reprs, out, fsActs, fsReprs) = f
    inputs = acts + reprs
    
    ninputs = len(inputs)
    tt = a.Interaction(inputs, out, 0.0).mkTruthTable(cells)
    intr = (acts, reprs, out)
    correct = evalTT(tt, intr, fsActs, fsReprs)

    ios = np.array([boolsToInt(list(p[0] + (p[1],))) for p in tt])
    ngenes = ios.shape[1]
    npats = ios.shape[0]
    
    ps = a.psGk(cells, list(inputs + (out,)))
    score = getScoreMI(tt, ps, intr, fsActs, fsReprs)
    percCorrect = getScore(tt, correct, ps) * 100
    
    xlabels = list(inputs + (out,))
    ylabels = [infoP(correct, ios[p, :]) + str(round(ps[tuple(ios[p, :])], 2))
               for p in xrange(npats)]
    
    axs.matshow(ios,cmap=plt.cm.gray_r, alpha = 0.75, aspect='auto')
    xaxis = axs.xaxis
    yaxis = axs.yaxis
    
    xlocs = np.array([i for i in xrange(ngenes)])
    ylocs = np.array([i for i in xrange(npats)])

    xaxis.set_ticks(xlocs + 0.5, minor=True)
    xaxis.set(ticks=xlocs, ticklabels=xlabels)
    
    yaxis.set_ticks(ylocs + 0.5, minor=True)
    yaxis.set(ticks=ylocs, ticklabels=ylabels)
    
    axs.set_xlabel(bfToString(f) + "\n" +
                   str(round(percCorrect, 2)) + "\n"
                   "MI=" + str(round(score, 2)) + "\n"
                   )
    xaxis.grid(True, which='minor')
    yaxis.grid(True, which='minor')

    return

def plotBoolIntr(cells, intr, axs):
    (acts, reprs, out) = intr
    inputs = acts + reprs

    intr = a.Interaction(inputs, out, 0.0)
    tt = intr.mkTruthTable(cells)
    ninputs = len(intr.inputs)
    tt = intr.mkTruthTable(cells)
    ios = np.array([boolsToInt(list(p[0] + (p[1],))) for p in tt])
    ios = ios[np.argsort(np.sum(ios[:, :ninputs], axis=1)), ]
    ngenes = ios.shape[1]
    npats = ios.shape[0]
    
    ps = a.psGk(cells, list(intr.inputs + (intr.out,)))
    xlabels = list(intr.inputs + (intr.out, ))
    ylabels = [round(ps[tuple(ios[p, :])], 2) for p in xrange(npats)]
    
    axs.matshow(ios,cmap=plt.cm.gray_r, alpha = 0.75, aspect='auto')
    #axs.set_title(intr.out, fontweight='bold')
    xaxis = axs.xaxis
    yaxis = axs.yaxis
    
    xlocs = np.array([i for i in xrange(ngenes)])
    ylocs = np.array([i for i in xrange(npats)])

    xaxis.set_ticks(xlocs + 0.5, minor=True)
    xaxis.set(ticks=xlocs, ticklabels=xlabels)
    
    yaxis.set_ticks(ylocs + 0.5, minor=True)
    yaxis.set(ticks=ylocs, ticklabels=ylabels)

    if not isFunction(tt):
        print("not a function")
        axs.set_xlabel('not a function', color='r')
        
    if isFunction(tt):
        print('function')
        axs.set_xlabel('function', color='b')
    

    xaxis.grid(True, which='minor')
    yaxis.grid(True, which='minor')

    
def lor(a, b): return a or b

def land(a, b): return a and b

def boolToInt(b):
    if b: return 1
    else: return 0
    
def boolsToInt(bs):
    return [boolToInt(b) for b in bs]

def const2(a, b): return b

def folds1(vals, fs, iVal):
    acc = iVal
    for i, v in enumerate(vals):
        acc = fs[i](acc, v)
        
    return acc

def folds(vals, fs):
    fs = (const2, ) + fs
    
    return folds1(vals, fs, False)

def evalI(intr, vals, fsAct, fsRepr):
    acts = intr[0]
    reprs = intr[1]
    
    nActs = len(acts)
    nReprs = len(reprs)
    
    if vals[nActs:]:
        return (folds(vals[:nActs], fsAct) and
                not folds(vals[nActs:], fsRepr))
    else:
        return folds(vals[:nActs], fsAct)

    
def evalIOr(intr, vals, fsAct, fsRepr):
    acts = intr[0]
    reprs = intr[1]
    
    nActs = len(acts)
    nReprs = len(reprs)
    
    if vals[nActs:]:
        return (folds(vals[:nActs], fsAct) or
                not folds(vals[nActs:], fsRepr))
    else:
        return folds(vals[:nActs], fsAct)


    
def evalTT(tt, intr, fsAct, fsRepr):
    correct = []
    for i, o in tt:
        if evalI(intr, i, fsAct, fsRepr) == o:
            correct.append((i, o))
            
    return correct

def getScore(tt, correct, ps):
    acc = 0.0
    for i, o in correct:
        acc += ps[tuple(boolsToInt(i + (o, )))]
        
    return acc

def getScoreTT(tt, ps, intr, fsAct, fsRepr):
    correct = []
    acc = 0.0
    
    for i, o in tt:
        if evalI(intr, i, fsAct, fsRepr) == o:
            correct.append((i, o))

    for i, o in correct:
        acc += ps[tuple(boolsToInt(i + (o, )))]
        
    return acc

def hDist(ps, ps1):
    s = sum((math.sqrt(p) - math.sqrt(p1))**2
              for p, p1 in zip(ps, ps1))

    return 1/math.sqrt(2) * math.sqrt(s)

def klDist(ps, ps1):
    if ([1 for p in ps if p == 0] or
        [1 for p in ps1 if p == 0]): return 1.0
    
    return -sum(p*math.log(p1 / p, 2) for p, p1 in zip(ps, ps1))

def log0(x):
    if x == 0: return 0.0
    else: return math.log(x, 2)

def mi(ps, p1, p2):
    s = 0.0
    for v in [0, 1]:
        for v1 in [0, 1]:
            if p1[v] == 0 or p2[v] == 0 or ps[(v, v1)] == 0:
                s+= 0
            else:
                s += ps[(v, v1)] * log0(ps[(v, v1)] / (p1[v] * p2[v1]))

    return s

def getScoreMI(tt, ps, intr, fsAct, fsRepr):
    joint = dict([((v, v1), 0.0) for v in [0, 1] for v1 in [0, 1]])
    p1 = dict([(v, 0.0) for v in [0, 1]])
    p2 = dict([(v, 0.0) for v in [0, 1]])
    
    for i, o in tt:
        pred = evalI(intr, i, fsAct, fsRepr)
        joint[(boolToInt(o), boolToInt(pred))] += ps[tuple(boolsToInt(i + (o, )))]
        p1[boolToInt(o)] += ps[tuple(boolsToInt(i + (o, )))]
        p2[boolToInt(pred)] += ps[tuple(boolsToInt(i + (o, )))]

    return mi(joint, p1, p2)

def tsGetScoreMI(ts1, ts2, g):
    joint = np.zeros([2, 2])
    p1 = np.zeros(2)
    p2 = np.zeros(2)

    n = len(ts1.cells.keys())

    for cid in ts1.cells.keys():
        e1 = ts1.cells[cid].exprs[g]
        e2 = ts2.cells[cid].exprs[g]
        joint[(boolToInt(e1), boolToInt(e2))] += 1
        p1[boolToInt(e1)] += 1
        p2[boolToInt(e2)] += 1


    joint = joint / n
    p1 = p1 / n
    p2 = p2 / n

    return mi(joint, p1, p2)

def ts_bacc(ts1, ts2, g):
    pos = sum([1 for c in ts1 if c.exprs[g]])
    neg = sum([1 for c in ts1 if not c.exprs[g]])

    tp = 0
    tn = 0

    tpr = 0.0
    tnr = 0.0

    for c in ts1:
        if c.exprs[g] and ts2.cells[c.cid].exprs[g]:
            tp += 1

        if (not c.exprs[g]) and (not ts2.cells[c.cid].exprs[g]):
            tn += 1

    if not pos ==0:
        tpr = tp / pos
    else:
        tpr = 1.0

    if not neg == 0:
        tnr = tn / neg
    else:
        tnr = 1.0

    return 0.5*(tpr + tnr)

def getScoreBAcc(tt, ps, intr, fsAct, fsRepr):
    pos = sum([ps[tuple(boolsToInt(i + (o, )))] for i, o in tt if o])
    neg = sum([ps[tuple(boolsToInt(i + (o, )))] for i, o in tt if not o])

    tp = 0.0
    tn = 0.0

    tpr = 0.0
    tnr = 0.0
    
    for i, o in tt:
        if o and evalI(intr, i, fsAct, fsRepr):
            tp += ps[tuple(boolsToInt(i + (o, )))]

        if (not o) and (not evalI(intr, i, fsAct, fsRepr)):
            tn += ps[tuple(boolsToInt(i + (o, )))]

    if not pos == 0:
        tpr = tp / pos
    else:
        tpr = 1.0

    if not neg == 0:
        tnr = tn / neg
    else:
        tnr = 1.0

    return 0.5*(tpr + tnr)


def tsPercCorrect(ts1, ts2, g):
    correct = 0
    n = len(ts1.cells.keys())
    
    for cid in ts1.cells.keys():
        e1 = ts1.cells[cid].exprs[g]
        e2 = ts2.cells[cid].exprs[g]
        if e1 == e2:
            correct += 1

    return correct / n
        
def getScoreDistr(tt, ps, intr, fsAct, fsRepr):
    psR = [0.0, 0.0]
    psF = [0.0, 0.0]
    
    for i, o in tt:
        pred = evalI(intr, i, fsAct, fsRepr)
        psF[boolToInt(pred)] += ps[tuple(boolsToInt(i + (o, )))]

    for i, o in tt:
        psR[boolToInt(o)] += ps[tuple(boolsToInt(i + (o, )))]

    return (1 - klDist(psR, psF))

def boolTerms(elems):
    #generator for all boolean expression (conjunctions/disjunctions) between elems
    nElems = len(elems)
    for fs in itertools.product(*itertools.repeat([lor, land], nElems-1)):
        for pelem in itertools.permutations(elems):
            yield (pelem, fs)


def rm_scores(f):
    (acts, reprs, out, fsAct, fsRepr, _, _) = f

    return (acts, reprs, out, fsAct, fsRepr)

def rm_tt(f):
    (acts, reprs, out, fsAct, fsRepr, score, _) = f

    return (acts, reprs, out, fsAct, fsRepr, score)

def checkFs(intr, cells, obj=getScoreBAcc):
    acts = intr[0]
    reprs = intr[1]
    out = intr[2]
    
    nActs = len(acts)
    nReprs = len(reprs)
    
    fss = []
    for pacts, fsAct in boolTerms(acts):
        for preprs, fsRepr in boolTerms(reprs):
            inputs = pacts + tuple(preprs)
            tt = a.Interaction(inputs, out, 0.0).mkTruthTable(cells)
            ps = a.psGk(cells, list(inputs) + [out])
            correct = evalTT(tt, intr, fsAct, fsRepr)
            fss.append((pacts,
                        tuple(preprs),
                        out,
                        fsAct,
                        fsRepr,
                        obj(tt, ps, intr, fsAct, fsRepr),
                        correct))

    return list(map(rm_tt,
                    sorted(fss, key=lambda x: x[5], reverse=True)))

def checkFs1(intr, cells):
    fs = checkFs(intr, cells)
    return fs[0] + ("",)

def fToString(f):
    if 'land' in repr(f): return u'\u2227'
    else: return u'\u2228'

def bfToString(f):
    (bacts, breprs, out, fsActs, fsReprs) = f

    factss = [fToString(f) for f in fsActs]
    freprss = [fToString(f) for f in fsReprs]

    formula_str = ""
    actss = ""
    reprss = ""
    
    if bacts:
        actss = "".join([relabelAnnot(val) for pair in zip(bacts, factss) for val in pair] + [bacts[-1]])
        formula_str += actss

    if breprs:
        reprss = "".join([relabelAnnot(val) for pair in zip(breprs, freprss) for val in pair] + [breprs[-1]])
        formula_str += ('\n\u00AC' + reprss)
    
    return formula_str

def updAct(intr, g):
    acts, reprs, out = intr

    return (acts+(g,), reprs, out)

def updRepr(intr, g):
    acts, reprs, out = intr

    return (acts, reprs+(g,), out)

def remT(t, rel):
    return tuple(el for el in t if not el == rel)

def searchIntrs(intr, cells, geneNms, updIntr):
    G = set(geneNms)
    I = set(intr[0] + intr[1] + (intr[2], ))

    fs = []
    for g in list(set.difference(G, I)):
        acts, reprs, out = intr
        intrG = updIntr(intr, g) 

        f = checkFs(intrG, cells)[0]
        fs.append(f)

    #return max(fs, key=lambda x: x[5])
    return fs

def searchIntrsNeg(intr, cells):
    acts, reprs, out = intr

    fs = []
    #remove acts
    for g in acts:
        intrG = (remT(acts, g), reprs, out)
        f = checkFs(intrG, cells)[0]
        fs.append(f)

    #remove reprs
    for g in reprs:
        intrG = (acts, remT(reprs, g), out)
        f = checkFs(intrG, cells)[0]
        fs.append(f)

    return fs

def tinds(t, inds):
    return [t[i] for i in inds]


def evalB(f, vals):
    (acts, reprs, out, fsAct, fsRepr) = f
    intr = (acts, reprs, out)


    return evalI(intr, vals, fsAct, fsRepr)

def evalB_d(f, actVals, reprVals):
    (acts, reprs, out, fsAct, fsRepr) = f
    intr = (acts, reprs, out)

    vals = [actVals[act] for act in acts] + [reprVals[repr] for repr in reprs]

    return evalI(intr, vals, fsAct, fsRepr)

def eq_tt1(f, f1):
    #assumes same variables
    from itertools import product, repeat

    (acts, reprs, out, fsAct, fsRepr) = f

    nActs = len(acts)
    nReprs = len(reprs)

    for vals in list(product(*repeat([0, 1], nActs+nReprs))):
        actVals = dict([(act, val) for act, val in zip(acts, vals[:nActs])])
        reprVals = dict([(rpr, val) for rpr, val in zip(reprs, vals[nActs:])])

        res = evalB_d(f, actVals, reprVals)
        res1 = evalB_d(f1, actVals, reprVals)

        if res != res1: return False

    return True
    

def eq_tt(f, f1):
    from itertools import product, repeat
    fEvals = []
    f1Evals = []

    nActs = len(acts(f))
    nReprs = len(reprs(f))

    nActs1 = len(acts(f1))
    nReprs1 = len(reprs(f1))

    if not nActs == nActs1 or not nReprs == nReprs1:
        return False
    
    for vals in list(product(*repeat([0, 1], nActs+nReprs))):
        actVals = vals[:nActs]
        reprVals = vals[nActs:]

        valsF = (tinds(actVals, np.argsort(np.argsort(acts(f)))) +
                  tinds(reprVals, np.argsort(np.argsort(reprs(f)))))

        valsF1 = (tinds(actVals, np.argsort(np.argsort(acts(f1)))) +
                  tinds(reprVals, np.argsort(np.argsort(reprs(f1)))))

        fEvals.append(evalB(f, valsF))
        f1Evals.append(evalB(f1, valsF1))

    return fEvals == f1Evals

def uniq_fs(fs):
    ufs = []
    for (i, f) in enumerate(fs):
        if not any([eq_tt(uf, f) for uf in ufs]):
            ufs.append(f)

    return ufs

def nb_fs(items, eq=eq_tt1):
    nb_items = []
    for i, item in enumerate(items):
        print(i)
        if any([eq(item, ui) for ui in nb_items]): continue
        else: nb_items.append(item)
            
    return nb_items

def searchIntrsD1(intr, cells, geneNms=geneNms):
    return sorted((searchIntrs(intr, cells, geneNms, updAct) +
                   searchIntrs(intr, cells, geneNms, updRepr) +
                   searchIntrsNeg(intr, cells)),
                  key=lambda x: x[5], reverse=True)

def rm_score(f):
    (acts, reprs, out, fsAct, fsReprs, _) = f
    return (acts, reprs, out, fsAct, fsReprs)
    

def score(f):
    (_, _, _, _, _, score) = f
    return score
    
def maxs_fs(fs, n=2, key=score):
    from itertools import groupby
    
    gs = []
    m = groupby(sorted(fs, key=key, reverse=True), key)
    for v, g in m:
        gs.append(list(g))

    return [f for f in reduce(lambda x, y: x+y, gs[:n])]

def max1_fs(fs, key=score, eps=0.1):
    m = max(fs, key=key)

    return [f for f in fs if (abs(score(f) - score(m)) / score(m)) < eps]

def merge_bterms(fsT, g, mf, ts=[10, 40, 96, 120, 132]):
    mfsT = []
    for t in ts:
        fs1 = list(map(rm_score, mf(fsT[t][g])))
        #fs1 = mf(fsT[t][g])
        mfsT.append(set(fs1))

    if mfsT: return reduce(set.intersection, mfsT)
    else: return set()

def merge_hyps(fsT, fsTRef, g, mf, ts=[10, 40, 96, 120, 132]):
    top_hyps = []
    for t in ts:
        top_fs = [rm_score(f) for f in max1_fs(fsT[t][g])]
        top_hypsT = set([diffBF(rm_score(fsTRef[t][g][0]), f) for f in top_fs])

        top_hyps.append(top_hypsT)

    if top_hyps: return reduce(set.intersection, top_hyps)
    else: return set()


def is_expr(ts, g):
    return sum([1 for c in ts if c.exprs[g]]) > 0

def ts_expr(tss, g):
    return [t for t, ts in tss.items() if is_expr(ts, g)]

def goSim(fnInit, fnFinal, nsteps):
    d = "../data/organism"
    dExprs = "../data/geneExpression"
    G = grn.GRN("../data/organism/modelFiles/refNet2.txt")

    cells = a.readDataT(d=d, dExprs=dExprs, t=132)
    intrs = grn.getInteractionsGenesT(a.geneNms, G)
    gintrs = getFs(intrs, cells)

    cellsToOrg(fnInit, cells)
    
    for i in xrange(nsteps):
        cells = updateTissue(gintrs, cells)

    cellsToOrg(fnFinal, cells)


def mkFnDat(t):
    return "out_t" + str(t) + "_data.data"

def mkFnRef(t):
    return "out_t" + str(t) + "_ref.data"

def mkFnHyp(t):
    return "out_t" + str(t) + "_hyp.data"

def mkFnRefFs(t):
    return "net_" + str(t) + "_ref"

def mkFnHypFs(t):
    return "net_" + str(t) + "_hyp"

def reprF(f, out):
    return out + " = " + bfToString(f)
    
def writeFs(fs, fnOut):
    with open(fnOut, 'w') as fOut:
        fOut.write('\n'.join([reprF(f, g) for g, f in fs.items()]))

    return

def goSimDatTs(fs, fs1, outDir):
    d = "../data/organism"
    dExprs = "../data/geneExpression"
    timepoints = [132]
    G  = grn.GRN("../data/organism/modelFiles/refNet2.txt")
    intrs = grn.getInteractionsGenesT(a.geneNms, G)

    for t in  timepoints:
        print(t)
        cells = a.readDataT(d=d, dExprs=dExprs, t=t)

        fnDat = join(outDir, mkFnDat(t))
        fnOut = join(outDir, mkFnRef(t))
        fnOut1 = join(outDir, mkFnHyp(t))
        
        cellsToDat(fnDat, cells)
        cellsToDat(fnOut, updateTissue(fs, cells))
        cellsToDat(fnOut1, updateTissue(fs1, cells))

        writeFs(fs, join(outDir, mkFnRefFs(t)))
        writeFs(fs1, join(outDir, mkFnHypFs(t)))

    return

def toNewmanFn(fn):
    (fn, ext) = os.path.splitext(fn)
    return fn + "000" + ".tif"

def goVisPatternsTs(confFile, outDir, visDir):
    timepoints = [132]
    visCmd = "/home2/Organism/tools/plot/bin/newman"
    mergeCmd = "convert"
    
    for t in  timepoints:
        fnDat = join(outDir, mkFnDat(t))
        fnOut = join(outDir, mkFnRef(t))
        fnOut1 = join(outDir, mkFnHyp(t))
        visDirT = join(visDir, "t"+str(t))
        if not os.path.exists(visDirT):
            os.makedirs(visDirT)
        for i, gene in enumerate(a.geneNms):
            print(i, gene)
            dataFn = join(visDirT, gene+"_data")
            refFn = join(visDirT, gene+"_ref")
            hypFn = join(visDirT, gene+"_hyp")
            #draw data pattern
            call([visCmd,
                  "-shape", "sphereVolume",
                  "-d", "3",
                  fnDat,
                  "-column", str(i+1),
                  "-output", "tiff",
                  dataFn,
                  "-camera", confFile,
                  "-min", str(0),
                  "-max", str(1)])
            
            #draw ref data pattern
            call([visCmd,
                  "-shape", "sphereVolume",
                  "-d", "3",
                  fnOut,
                  "-column", str(i+1),
                  "-output", "tiff",
                  refFn,
                  "-camera", confFile,
                  "-min", str(0),
                  "-max", str(1)])

            #draw hyp data pattern
            call([visCmd,
                  "-shape", "sphereVolume",
                  "-d", "3",
                  fnOut1,
                  "-column", str(i+1),
                  "-output", "tiff",
                  hypFn,
                  "-camera", confFile,
                  "-min", str(0),
                  "-max", str(1)])

            #merge
            call([mergeCmd,
                  toNewmanFn(dataFn),
                  toNewmanFn(refFn),
                  toNewmanFn(hypFn),
                  "+append",
                  "-pointsize", "20",
                  "-fill", "white",
                  "-annotate", "+10+40", gene,
                  join(visDirT, gene+".tif")])

            #delete individual images
            os.remove(toNewmanFn(dataFn))
            os.remove(toNewmanFn(refFn))
            os.remove(toNewmanFn(hypFn))
            

def goPlotFs(fs):
    d = "../data/organism"
    dExprs = "../data/geneExpression"
    cells = a.readDataT(d=d, dExprs=dExprs, t=132)

    fig, axs = plt.subplots(nrows=4, ncols=5)
    axs = axs.reshape(-1)
    fig.tight_layout()

    fsR = [f for g, f in fs.items() if acts(f) or reprs(f)]
    
    for i, f in enumerate(fsR):
        plotBoolF(cells, f, axs=axs[i])

    return

def goPlotFs1(fs, fs1):
    d = "../data/organism"
    dExprs = "../data/geneExpression"
    cells = a.readDataT(d=d, dExprs=dExprs, t=132)

    fig, axs = plt.subplots(nrows=4, ncols=5)
    axs = axs.reshape(-1)
    fig.tight_layout()

    fsR = dict([(g, f) for g, f in fs.items() if acts(f) or reprs(f)])

    for i, gf in enumerate(fsR.items()):
        g, f = gf
        plotBoolF1(cells, f, fs1[g], axs=axs[i])

    plt.show()

    return

def plotPatterns(baseDir):
    import os
    from subprocess import call
    mergeCmd = "convert"

    for f in os.listdir(baseDir):
        (fn, ext) = os.path.splitext(f)
        if fn.endswith("_in"):
            geneNm = fn.split("_")[0]
            fn1 = os.path.join(baseDir, f)
            fn2 = os.path.join(baseDir, geneNm + "_out.png")
            mergedFile = os.path.join(baseDir, geneNm.upper() + ".png")
            call([mergeCmd,
                  fn1,
                  fn2,
                  "+append",
                  "-pointsize", "20",
                  "-fill", "white",
                  "-annotate", "+10+40", geneNm.upper(),
                  mergedFile])

def get_best(fsT):
    fsT_best = dict()
    for t, fs in fsT.items():
        fsT_best[t] = dict()
        for g, bs in fs.items():
            fsT_best[t][g] = rm_score(max(fsT[t][g], key=lambda x: x[5]))

    return fsT_best

def plotTissueExpr(ts, g):
    xs = [c.pos.x for c in ts]
    ys = [c.pos.y for c in ts]
    zs = [c.pos.z for c in ts]

    cs = ['red' if c.exprs[g] else 'blue' for c in ts]

    fig = plt.figure()                                          
    ax = fig.add_subplot(111, projection='3d')
    ax.view_init(elev=95, azim=270)
    ax.scatter(xs, ys, zs, c=cs, alpha=0.9, s=200,
               edgecolors='black', linewidths=0.5)
    
    plt.show()

def get_fsT(tss, intrs, tpoints=[10, 40, 96, 120, 132]):
    fs = dict()
    for t in tpoints:                                                                                          
        print(t)                                                                      
        fs[t] = getFsAll(intrs, list(tss[t]), checkFs)

    return fs


def get_fsTD1(tss, intrs, tpoints=[10, 40, 96, 120, 132]):
    fsD1 = dict()
    for t in tpoints:                                                                                          
        print(t)                                                                      
        fsD1[t] = getFsAll(intrs, list(tss[t]), searchIntrsD1)

    return fsD1


def getCons(g, fsT, tpoints=[10, 40, 96, 120, 132]):
    fsG = []
    for i in range(len(tpoints)):
        fsG.append(merge_bterms(fsT, g, max1_fs,
                                ts=tpoints[(-(i+1)):]))

    fsCons = [(t, fs) for t, fs in zip(tpoints, reversed(fsG)) if fs]

    (t, fs_) = fsCons[0]
    
    return (t, nb_fs(fs_))

def tuplify(xs):
    return tuple(tuple(x) for x in xs)

def merge_hyps(fsT, fsTRef, g, mf, ts=[10, 40, 96, 120, 132]):
    top_hyps = []
    for t in ts:
        top_fs = [rm_score(f) for f in max1_fs(fsT[t][g])]
        top_hypsT = set([tuplify(diffBF(rm_score(fsTRef[t][g][0]), f)) for f in top_fs])

        top_hyps.append(top_hypsT)

    if top_hyps: return reduce(set.intersection, top_hyps)
    else: return set()
    
def get_cons_hyps(fsT, fsTRef, g, ts=[10, 40, 96, 120, 132]):
    fsG = []
    for i in range(len(ts)):
        fsG.append(merge_hyps(fsT, fsTRef, g, max1_fs,
                                ts=ts[(-(i+1)):]))

    fsCons = [(t, fs) for t, fs in zip(ts, reversed(fsG)) if fs]
    
    return fsCons[0]

def relabelAnnot(gn):
    if gn == "CUC1_2_3": return "CUC"
    elif gn == "AP1_2": return "AP1/2"
    elif gn == "PHB_PHV": return "PHB"
    elif gn == "ETTIN": return "ETT"
    else: return gn


def lookupF(fs, f):
    for f_ in fs:
        if f_[:-1] == f:
            return score(f_)
        
    return None

def reportFs(g, fsRef, maxToReport=5, tpoints=[10, 40, 96, 120, 132]):
    for t in tpoints:
        print(t)
        fs = max1_fs(fsRef[t][g])
        fs_ = uniq_fs(map(rm_score, fs))

        for f in fs_[:maxToReport]:
            print(bfToString(f))
            print(round(lookupF(fs, f), 3))

        if len(fs_) - maxToReport > 0:
               print("(+"+str(len(fs_) - maxToReport), "others)")

        print("--------")

        
    (t, fsCons) = getCons(g, fsRef)

    for f in fsCons:
        print(bfToString(f))
        print("(" + ", ".join([str(tp)+"h" for tp in tpoints[tpoints.index(t):]]) + ")")

    return

def report_fs_csv(g, fsRef, maxToReport=5, tpoints=[10, 40, 96, 120, 132]):
    gRow = "\"" + g + "\""
    
    for t in tpoints:
        fs = max1_fs(fsRef[t][g])
        fs_ = list(map(rm_score, fs))

        tCell = "\"" + "\n".join([bfToString(f) + "\n" + str(round(lookupF(fs, f), 3))
                                for f in fs_[:maxToReport]])

        if len(fs_) - maxToReport > 0:
               tCell += ("\n" +
                         "(+" +
                         str(len(fs_) - maxToReport) +
                         " others)")

        gRow += ("," + tCell + "\"")


    (t, fsCons) = getCons(g, fsRef)

    summCell = "\""
    for f in fsCons:
        summCell += bfToString(f)
        summCell += "\n" + ("(" +
                            ", ".join([str(tp)+"h" for tp in tpoints[tpoints.index(t):]])
                            + ")") + "\n"

    summCell += "\""
    gRow += ("," + summCell)

    return gRow


def reportFsD1(g, fsRef, fsT, maxToReport=5, tpoints=[10, 40, 96, 120, 132]):
    for t in tpoints:
        print(t)
        fs = max1_fs(fsT[t][g])
        print(len(fs))
        for f in fs[:maxToReport]:
            dActs, dReprs = diffBF(rm_score(fsRef[132][g][0]), rm_score(f))
            if dActs:
                print(relabelAnnot(list(dActs)[0]), round(score(f), 3))
            elif dReprs:
                print("!", relabelAnnot(list(dReprs)[0]), round(score(f), 3))
        print("(+"+str(len(fs) - maxToReport), "others)")
        print("--------")

    (t, fsCons) = getCons(g, fsT)
    print(t)
    for f in fsCons:
        dActs, dReprs = diffBF(rm_score(fsRef[132][g][0]), f)
        if dActs:
            print(relabelAnnot(list(dActs)[0]))
        elif dReprs:
            print("!", relabelAnnot(list(dReprs)[0]))
        
    return

def report_fsD1_csv(g, fsRef, fsT, maxToReport=5,
                    tpoints=[10, 40, 96, 120, 132]):
    gRow = "\"" + g + "\""
    for t in tpoints:
        fs = max1_fs(fsT[t][g])
        fs_ = [rm_score(f) for f in fs]
        tCell = "\""

        for f in fs[:maxToReport]:
            dActs, dReprs, dns = diffBF(rm_score(fsRef[132][g][0]), rm_score(f))
            if dActs:
                tCell += ("\n" +
                          relabelAnnot(list(dActs)[0]) +
                          " " +
                          str(round(score(f), 3)))
            elif dReprs:
                tCell += ("\n" +
                          '\u00AC' +
                          relabelAnnot(list(dReprs)[0]) +
                          " " +
                          str(round(score(f), 3)))
            elif dns:
                tCell += ("\n" +
                          '--' +
                          relabelAnnot(list(dns)[0]) +
                          " " +
                          str(round(score(f), 3)))
                
        if len(fs) - maxToReport > 0:
            tCell += ("\n" + "(+" + str(len(fs) - maxToReport) + " others)")

        tCell += "\""
        gRow += ("," + tCell)

    (t, hyps) = get_cons_hyps(fsT, fsRef, g)
    summCell = "\""
    for hyp in hyps:
        dActs, dReprs, dns = hyp
        if dActs:
            summCell += ("\n" + relabelAnnot(list(dActs)[0]))
        elif dReprs:
            summCell += ("\n" + '\u00AC' + relabelAnnot(list(dReprs)[0]))
        elif dns:
            summCell += ("\n" + '--' + relabelAnnot(list(dns)[0]))

    summCell += "\n" + ("(" +
                        ", ".join([str(tp)+"h" for tp in tpoints[tpoints.index(t):]])
                        + ")") + "\n"

    summCell += "\""
    gRow += ("," + summCell)

    return gRow

def serialise_bf(f):
    if 'land' in repr(f): return 'and'
    elif 'lor' in repr(f): return 'or'
    else: raise ValueError("error")

def deserialise_bf(f):
    if f == 'and': return land
    elif f == 'or': return lor
    else: raise ValueError("error")

def serialise_f(bexpr):
    return (bexpr[:3] +
            (tuple(serialise_bf(f) for f in bexpr[3]),
             tuple(serialise_bf(f) for f in bexpr[4])) +
            bexpr[5:])

def deserialise_f(bexpr):
    return (bexpr[:3] +
            (tuple(deserialise_bf(f) for f in bexpr[3]),
             tuple(deserialise_bf(f) for f in bexpr[4])) +
            bexpr[5:])

def map_gfs(gfs, sf):
    return dict([(g, list(map(sf, fs)))
                   for g, fs in gfs.items()])

def map_fst(fsT, sf):
    return dict([(t, map_gfs(gfs, sf))
                  for t, gfs in fsT.items()])
    
