from __future__ import print_function
from collections import defaultdict
from operator import add
from functools import reduce
from itertools import combinations, permutations
from pprint import pprint
import numpy as np
import networkx as nx
import common.seg as seg

geneNms = [
    'AP3',
    'REV',
    'SUP',
    'AHP6',
    'PHB_PHV',
    'SEP1_2',
    'PI',
    'LFY',
    'AS1',
    'AG',
    'ATML1',
    'STM',
    'AP1_2',
    'L2',
    'L1',
    'ETTIN',
    'SEP3',
    'FIL',
    'WUS',
    'PUCHI',
    'ANT',
    'CUC1_2_3',
    'MP',
    'CLV3',
    ]

geneFs = {
    'AUX': 'growth',
    'AP3': 'identity',
    'REV': 'polarity',
    'SUP': 'meristem',
    'AHP6': 'growth',
    'PHB_PHV': 'polarity',
    'SEP1_2': 'identity',
    'PI': 'identity',
    'LFY': 'growth',
    'AS1': 'polarity',
    'AG': 'identity',
    'STM' : 'meristem',
    'AP1_2': 'identity',
    'ETTIN': 'polarity',
    'SEP3' : 'identity',
    'FIL' : 'polarity',
    'WUS': 'meristem',
    'PUCHI' : 'growth',
    'ANT' : 'growth',
    'CUC1_2_3': 'meristem',
    'MP' : 'growth',
    'CLV3': 'meristem',
    'KAN1': 'polarity',
    'CLV1': 'meristem'
    }

class Interaction(object):
    def __init__(self, inputs, out, w):
        self.inputs = inputs
        self.out = out
        self.w = w
        self.tt = None

    def __repr__(self):
        inputsRepr = ",".join(self.inputs)
        
        return inputsRepr + " --> " + self.out

    def _getExprs(self, c):
        inputExprs = tuple([c.exprs[i] for i in self.inputs])
        outExpr = c.exprs[self.out]

        return (inputExprs, outExpr)
        
    def mkTruthTable(self, cells):
        #get truth table per interaction, which can then be compared to
        #known truth table to get the interactions rules
        ttable = set(self._getExprs(c) for c in cells)
        self.tt = ttable
        return ttable

    def partOf(self, i, o):
        return i in self.inputs and self.out == o

    def toPairs(self):
        return [(i, self.out) for i in self.inputs]
    
    def toWPairs(self):
        return [(i, self.out, {'weight':self.w, 'tt':self.tt}) for i in self.inputs]
        

def readDataT(d, dExprs, t):
    from os.path import join
    
    geomF = "_segmented_tvformat_0_volume_position.txt"
    neighF = "_segmented_tvformat_0_neighbors_py.txt"
    exprF = "h.txt"

    fnGeom = join(d, "t" + str(t) + geomF)
    fnNeigh = join(d, "t" + str(t) + neighF)
    fnExpr = join(dExprs, "t_" + str(t) + exprF)
    
    gexprs = seg.readExprs(fnExpr)
    ts = seg.STissue.fromTV(fnGeom, fnNeigh, gexprs, seg.geneNms)

    return list(iter(ts))

def readDataT1(d, dExprs, t):
    from os.path import join
    
    geomF = "_segmented_tvformat_0_volume_position.txt"
    neighF = "_segmented_tvformat_0_neighbors_py.txt"
    exprF = "h.txt"

    fnGeom = join(d, "t" + str(t) + geomF)
    fnNeigh = join(d, "t" + str(t) + neighF)
    fnExpr = join(dExprs, "t_" + str(t) + exprF)
    
    gexprs = seg.readExprs(fnExpr)
    ts = seg.STissue.fromTV(fnGeom, fnNeigh, gexprs, seg.geneNms)

    return ts

def readDataT1Im(d, dExprs, t):
    from os.path import join

    exprF = "h.txt"
    fnRes = join(d, "resolutions.txt")
    fnSeg = join(d, "segmentations/t"+str(t)+"_segmented.tif")
    fnExpr = join(dExprs, "t_" + str(t) + exprF)
    
    
    data, _ = seg.readImage(fnSeg)
    ress = seg.readRess(fnRes)
    res = ress[t]

    gexprs = seg.readExprs(fnExpr)
    ts = seg.STissue.fromImage(data, res, gexprs, seg.geneNms)

    for c in ts:
        c.swapZX()

    return ts

def readData(d, dExprs):
    timepoints = [10, 40, 96, 120, 132]
    cells = []

    for t in timepoints:
        print(t)
        cells.append(readDataT(d, dExprs, t))

    return reduce(add, cells)

def initPs(geneNms, k, val):
    ps = dict()
    for genes in combinations(geneNms, k):
        ps[frozenset(genes)] = np.copy(val)

    return ps

def initM(geneNms, outGs, k):
    ps = dict()
    for genes in combinations(geneNms, k):
        ps[frozenset(genes)] = dict((g, 0.0) for g in outGs)

    return ps

def ps(cells, km):
    P = dict()
    N = len(cells)
    
    for k in range(1, km+2):
        P[k] = initPs(geneNms, k, np.zeros(tuple(2 for _ in range(k))))

    for k in range(1, km+2):
        for cell in cells:
            for gks in combinations(geneNms, k):
                counts = P[k][frozenset(gks)]
                gkExprs = tuple([int(cell.exprs[g]) for g in list(gks)])
                counts[gkExprs] += 1
            
    for k in range(1, km+2):
        for gks in combinations(geneNms, k):
            P[k][frozenset(gks)] /= N
        
    return P

def psGk(cells, gks):
    k = len(gks)
    N = len(cells)
    counts = np.zeros(tuple(2 for _ in range(k)))
    
    for cell in cells:
        gkExprs = tuple([int(cell.exprs[g]) for g in list(gks)])
        counts[gkExprs] += 1

    return (counts / N)

def entropy(P):
    return -np.sum(P * np.log(P, out=np.zeros_like(P), where=(P!=0)))

def mkEntropies(P, km):
    H = dict()
    for k in range(1, km+2):
        H[k] = initPs(geneNms, k, 0.0)

    for k in range(1, km+2):
        for gks in combinations(geneNms, k):
            H[k][frozenset(gks)] = entropy(P[k][frozenset(gks)])

    return H

def mutualInfo(H, outGs, k):
    hOut = H[1]
    hIn = H[k]
    hJoint = H[k+1]
    
    M = initM(geneNms, outGs, k)
    
    for inputs in combinations(geneNms, k):
        for out in outGs:
            if not out in inputs:
                inputOut = frozenset(inputs + (out, ))
                M[frozenset(inputs)][out] = (hOut[frozenset([out])]
                                            + hIn[frozenset(inputs)]
                                            - hJoint[inputOut])

    return M

def getOuts(interactions):
    return [intr.out for intr in interactions]

def getInteractions(M, H, outGs, k, s):
    hOut = H[1]
    interactions = []
    
    for inputs in combinations(geneNms, k):
        for out in outGs:
            if (not out in inputs
                and ((M[frozenset(inputs)][out]/ hOut[frozenset([out])]) >= s)
                and not hOut[frozenset([out])] == 0):
                    w = M[frozenset(inputs)][out]/ hOut[frozenset([out])]
                    interactions.append(Interaction(inputs, out, w))

    return interactions

def go(cells, kmax, s):
    out = set(geneNms)
    outDet = set([])
    
    P = ps(cells, kmax)
    H = mkEntropies(P, kmax)

    interactions = []
    
    for k in range(1, kmax+1):
        out = out.difference(outDet)
        M = mutualInfo(H, out, k)
        intrs = getInteractions(M, H, out, k, s)
        interactions += intrs
        outDet = getOuts(intrs)

    return (P, interactions)

def printTTable(ttable):
    ios = list(ttable)
    
    for (inputs, out) in ios:
        print(",".join(str(int(i)) for i in inputs) + " --> " + str(int(out)))

    return

def satTT(tt, ttIntr):
    return all([(inputs, out) in tt for inputs, out in ttIntr])

def checkIntr(intr, cells):
    idTT = {((False,), False), ((True, ), True)}
    notTT = {((False, ), True), ((True,), False)}

    orTT = {((False, False), False), ((False, True), True),
            ((True, False), True), ((True, True), True)}
    xorTT = {((False, False), False), ((False, True), True),
            ((True, False), True), ((True, True), False)}
    andTT = {((False, False), False), ((False, True), False),
             ((True, False), False), ((True, True), True)}

    ttNms = ['ID', 'NOT', 'OR', 'XOR', 'AND']
    tts = [idTT, notTT, orTT, xorTT, andTT]
    ttIntr = intr.mkTruthTable(cells)
    
    return [ttNms[i] for i, tt in enumerate(tts) if satTT(tt, ttIntr)]
    

def printInteractions(interactions, cells):
    for intr in interactions:
        print(intr)
        print(",".join(checkIntr(intr, cells)))
        printTTable(intr.tt)
        print()

    return

def concatMap(xs, f):
    return reduce(add, map(f, xs), [])

def toGraph(interactions, cells):
    import networkx as nx
        
    for intr in interactions:
        intr.mkTruthTable(cells)

    G = nx.DiGraph()
    G.add_edges_from(concatMap(interactions, lambda i: i.toWPairs()))

    return G

def mkWGraphRef(interactions, ref, cells):
    import networkx as nx
    #draw the ref graph with weights from the data-extracted interactions
    G = toGraph(interactions, cells)

    wRef = []
    for i, o in ref:
        try:
            wRef.append((i, o, G.adj[i][o]['weight']))
        except KeyError:
            wRef.append((i, o, 0.0))

    Gref = nx.DiGraph()
    Gref.add_weighted_edges_from(wRef)

    return Gref

def filterRef(geneNms, ref):
    genes = set(geneNms)
    
    return [(i, o) for i, o in ref if i in genes and o in genes]

def createRefData(geneNms, k):
    return [(g1, g2) for g1, g2 in permutations(geneNms, k) if not g1 == g2]

def drawGraph(G):
    import networkx as nx
    import matplotlib.pyplot as plt
    import matplotlib as mpl
    
    pos = nx.layout.circular_layout(G)

    nodes = nx.draw_networkx_nodes(G, pos, node_size=700)
    edges = nx.draw_networkx_edges(G, pos, width=2)
    nx.draw_networkx_labels(G, pos, font_size=14, font_family='sans-serif')
    
    ax = plt.gca()
    ax.set_axis_off()
    plt.show()

def drawWGraph(G, w0=False):
    import networkx as nx
    import matplotlib.pyplot as plt
    import matplotlib as mpl
    
    ws = nx.get_edge_attributes(G, 'weight')

    if w0:
        ews = [ws[e] for e in G.edges() if ws[e] > 0.0]
        edgesToDraw = [e for e in G.edges() if ws[e] > 0.0]
    else:
        ews = [ws[e] for e in G.edges()]
        edgesToDraw = G.edges()
        
    pos = nx.layout.circular_layout(G)
    
    print(ews)
    print(edgesToDraw)
    
    nodes = nx.draw_networkx_nodes(G, pos, node_size=500, alpha=0.6)
    edges = nx.draw_networkx_edges(G, pos, edge_list=edgesToDraw, edge_color=ews,
                                   edge_cmap=plt.cm.Blues, width=2)
    nx.draw_networkx_labels(G, pos, font_size=10, font_family='sans-serif')
    
    pc = mpl.collections.PatchCollection(edges, cmap=plt.cm.Blues)
    pc.set_array(ews)
    plt.colorbar(pc)

    ax = plt.gca()
    ax.set_axis_off()
    plt.show()

def circularOrdered(nodes, centre=(0, 0), r=1):
    #like nx circular positioning but preserves the order of the nodes
    
    N = len(nodes)
    thetas = np.linspace(0, 2*np.pi, N+1)

    poss = np.zeros([N+1, 2])
    for (i, th) in enumerate(thetas):
        a, b = centre
        x = a + r*np.cos(th)
        y = b + r*np.sin(th)
        poss[i, :] = [x, y]
        
    return dict(zip(nodes, poss))

def getW(ws, p):
    a, b = p

    try:
        return ws[p]
    except KeyError:
        return ws[(b, a)]

    return

def rescale(w, p):
    d, h = p

    return w * (h - d) + d

def inRef(intr, refComps):
    return intr[0] in refComps or intr[1] in refComps

def drawRefData(interactions, interactionsC, cells, ref, geneF, refOnly=False, engine='circo'):
    import networkx as nx
    import matplotlib.pyplot as plt
    import matplotlib as mpl

    fColours = {'identity': '#FCFFE5',
                'growth': '#8BB9FF',
                'polarity': '#FFE1BB',
                'meristem': '#ED9EAE',
                '': '#FFFFFF'}

    sref = set(ref)
    Gref = mkWGraphRef(interactions, ref, cells)
    
    newIntrs = []
    for intr in interactionsC:
        if (any ([not bintr in sref and inRef(bintr, Gref.nodes())
                  for bintr in intr.toPairs()])):
            newIntrs.append(intr)

    nIntrPairs = concatMap(newIntrs, lambda i: i.toPairs())
    Gref.add_edges_from(nIntrPairs)

    ws = nx.get_edge_attributes(Gref, 'weight')
    ews = [rescale(getW(ws, e), (0.0, 1.0)) for e in ref]

    ncs = [fColours[geneF.get(g, '')] for g in Gref.nodes()]
    nodesF = map(lambda x: x[0],
                 sorted([(n, geneF.get(n, '')) for n in Gref.nodes()],
                        key=lambda p: p[1]))
    pos = nx.nx_agraph.graphviz_layout(Gref, prog=engine)#
    
    nodes = nx.draw_networkx_nodes(Gref, pos, node_color=ncs,
                                   alpha=0.9, node_size=700)
    if not refOnly:
        nx.draw_networkx_edges(Gref, pos, width=1, edge_color='r',
                               edgelist=nIntrPairs)
    wedges = nx.draw_networkx_edges(Gref, pos, width=1, edgelist=ref,
                                    edge_cmap=plt.cm.Blues, edge_color=ews,
                                    edge_vmin=0.0, edge_vmax=1.0)
    nx.draw_networkx_labels(Gref, pos, font_size=10)
    wsLabels = dict([(p, str(round(w, 1))) for (p, w) in ws.iteritems()])
    nx.draw_networkx_edge_labels(Gref, pos, edge_labels=wsLabels)
    

    pc = mpl.collections.PatchCollection(wedges, cmap=plt.cm.Blues)
    pc.set_array(ews)
    pc.set_clim(vmin=0.0, vmax=1.0)
    plt.colorbar(pc)

    ax = plt.gca()
#   ax.set_axis_off()
    ax.set_facecolor("lightslategray")
    plt.show()

    return (Gref, newIntrs)
    
def tAllRefData(ref):
    d = "../data/organism"                        
    dExprs = "../data/geneExpression" 
    timepoints = [10, 40, 96, 120, 132]

    for t in timepoints:
        print(t)
        cells = readDataT(d, dExprs, t)
        P, interactions = go(cells, 2, 0.1)
        _, interactionsC = go(cells, 2, 1.0)

        Gr, nIntrs = drawRefData(interactions, interactionsC, cells, ref, geneFs)

        fnOutG = "t" + str(t) + "_net.txt"
        fnOutN = "t" + str(t) + "_nIntrs.txt"
        nx.nx_agraph.write_dot(Gr, fnOutG)

        with open(fnOutN, 'w') as fout:
            for i in nIntrs:
                fout.write(str(i) + "\n")

    return

def tAllStatic(s, ref, w0=False):
    d = "../data/organism"                        
    dExprs = "../data/geneExpression" 
    timepoints = [10, 40, 96, 120, 132]

    for t in timepoints:
        print(t)
        cells = readDataT(d, dExprs, t)
        P, interactions = go(cells, 2, s)

        for intr in interactions:
            print(intr)
            print(intr.w)
            
        Gr = mkWGraphRef(interactions, ref, cells)
        drawWGraph(Gr)

    return

def tAll(s):
    d = "../data/organism"                        
    dExprs = "../data/geneExpression" 
    timepoints = [10, 40, 96, 120, 132]
    
    for t in timepoints:
        print(t)
        cells = readDataT(d, dExprs, t)
        P, interactions = go(cells, 2, s)

        G = toGraph(interactions, cells)

        if s == 1.0: drawGraph(G)
        else: drawWGraph(G)

    return

def isFunction(ios):
    pass
        
# to read specific timepoint (e.g.40)
# cells = readDataT("../data/organism", "../data/geneExpression", 40)
# to read all timepoints
# cells = readData("../data/organism", "../data/geneExpression")
#
# P, interactions = go(cells, 2) #for max number of inputs=2
# printInteractions(interactions, cells) #print interactions + truth tables for each
