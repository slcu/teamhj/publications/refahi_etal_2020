Compute and verify the voxel size estimate of Atlas time points
===============================================================

Uses computeL1Width method from atlasviewer.image. You need to install 
atlasviewer first.

To run the script update the segmented file paths `segmentedImagePath_10h`, 
`segmentedImagePath_40h`,  `segmentedImagePath_96h`, `segmentedImagePath_120h`,
`segmentedImagePath_132h`. 