import _pickle as cPickle
import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import aniso as ans
from collections import defaultdict

import common.seg as seg
import common.lin as lin
import common.statesN as f
import common.edict as edict

patsToClusters = {2 : 1,
                  3 : 1,
                  6 : 1,
                  7 : 1,
                  10 : 1,
                  18 : 2,
                  27 : 2,
                  1 : 3,
                  32 : 3,
                  5 : 3,
                  9 : 4,
                  17: 5,
                  29 : 5,
                  16 : 5,
                  28 :5,
                  11 : 6,
                  8 : 6,
                  12 : 7,
                  21 :8,
                  31:8,
                  20:8,
                  30:9,
                  19:10,
                  25:11,
                  13:11,
                  14:12,
                  15:12,
                  22:13,
                  26:14,
                  23:14,
                  24:14}

def findPattern1(cid, t, tss):
    c = tss[t].cells.get(cid, None)
    
    if c:
        return f.get_state(c.getOnGenes())
    else:
        return -1
    
def getGAnisos():
    import gzip
    fname = "../data/data_Jonathan/YR_01_ATLAS_iso-cell_features2.pkz"
    f = gzip.open(fname, 'r')
    feat_dict = cPickle.load(f)
    f.close()

    growth_anisotropy_3D_120_to_132 = dict()
    for k, v in feat_dict["growth_anisotropy_3D"].iteritems():
        if feat_dict["tp_index"][k] == 3:
            cid = feat_dict["vid2label"][k]
            growth_anisotropy_3D_120_to_132[cid] = v

    growth_anisotropy_3D_96_to_120 = dict()
    for k, v in feat_dict["growth_anisotropy_3D"].iteritems():
        if feat_dict["tp_index"][k] == 2:
            cid = feat_dict["vid2label"][k]
            growth_anisotropy_3D_96_to_120[cid] = v


    growth_anisotropy_3D_40_to_96 = dict()
    for k, v in feat_dict["growth_anisotropy_3D"].iteritems():
        if feat_dict["tp_index"][k] == 1:
            cid = feat_dict["vid2label"][k]
            growth_anisotropy_3D_40_to_96[cid] = v

    growth_anisotropy_3D_10_to_40 = dict()
    for k, v in feat_dict["growth_anisotropy_3D"].iteritems():
        if feat_dict["tp_index"][k] == 0:
            cid = feat_dict["vid2label"][k]
            growth_anisotropy_3D_10_to_40[cid] = v


    return {10 : addPats1(growth_anisotropy_3D_10_to_40, 10),
            40 : addPats1(growth_anisotropy_3D_40_to_96, 40),
            96 : addPats1(growth_anisotropy_3D_96_to_120, 96),
            120: addPats1(growth_anisotropy_3D_120_to_132, 120)}

def getGAnisosData():
    import gzip
    fname = "../data/data_Jonathan/YR_01_ATLAS_iso-cell_features2.pkz"
    f = gzip.open(fname, 'r')
    feat_dict = cPickle.load(f)
    f.close()

    growth_anisotropy_3D_120_to_132 = dict()
    for k, v in feat_dict["growth_anisotropy_3D"].iteritems():
        if feat_dict["tp_index"][k] == 3:
            cid = feat_dict["vid2label"][k]
            growth_anisotropy_3D_120_to_132[cid] = v

    growth_anisotropy_3D_96_to_120 = dict()
    for k, v in feat_dict["growth_anisotropy_3D"].iteritems():
        if feat_dict["tp_index"][k] == 2:
            cid = feat_dict["vid2label"][k]
            growth_anisotropy_3D_96_to_120[cid] = v


    growth_anisotropy_3D_40_to_96 = dict()
    for k, v in feat_dict["growth_anisotropy_3D"].iteritems():
        if feat_dict["tp_index"][k] == 1:
            cid = feat_dict["vid2label"][k]
            growth_anisotropy_3D_40_to_96[cid] = v

    growth_anisotropy_3D_10_to_40 = dict()
    for k, v in feat_dict["growth_anisotropy_3D"].iteritems():
        if feat_dict["tp_index"][k] == 0:
            cid = feat_dict["vid2label"][k]
            growth_anisotropy_3D_10_to_40[cid] = v

    return {10 : growth_anisotropy_3D_10_to_40,
            40 : growth_anisotropy_3D_40_to_96,
            96 : growth_anisotropy_3D_96_to_120,
            120: growth_anisotropy_3D_120_to_132}

def calcAnisotropies(regs, tss, linss):
    tpoints = sorted(tss.keys())

    gansF = dict([(t, ans.ganisos_forward(regs, tss, linss, t, lin.succ(t)))
                 for t in tpoints if lin.succ(t)])

    gansB = dict([(t, ans.ganisos_backward(regs, tss, linss, lin.prev(t), t))
                  for t in tpoints if lin.prev(t)])

    gansAvg = edict.unionWith(gansF, gansB,
                              lambda m1, m2: edict.unionWith(m1, m2, avg2))

    return gansAvg

def addCombPatterns(gans, tss):
    return edict.mapWithKey(gans, lambda t, ans: addPats1(ans, t, tss))

def getGAnisos1():
    tpoints = [10, 40, 96, 120]
    regs = seg.getCellRegions(d=3)
    tss, linss = lin.mkSeries()

    gans = dict([(t, ans.ganisos_forward(regs, tss, linss, t, lin.succ(t)))
                 for t in tpoints])

    return dict([(t, addPats1(gans[t], t)) for t in tpoints])

def getGAnisosRev1():
    tpoints = [40, 96, 120, 132]
    regs = seg.getCellRegions(d=3)
    tss, linss = lin.mkSeries()
    
    gans = dict([(t, ans.ganisos_backward(regs, tss, linss, lin.prev(t), t))
                 for t in tpoints])

    return dict([(t, addPats1(gans[t], t)) for t in tpoints])

def getGAnisosRevData():
    tpoints = [40, 96, 120, 132]
    tss, linss = lin.mkSeries()
    gansDataF = getGAnisosData()
    
    gans = dict([(t, ans.ganisos_backward_data(gansDataF, tss, linss, lin.prev(t), t))
                 for t in tpoints])

    return dict([(t, addPats1(gans[t], t)) for t in tpoints])


class TPat(object):
    def __init__(self, pid, t):
        self.pid = pid
        self.t = t
        self.gr = 0
        self.ga = 0

    def addGrowthRate(self, gr):
        self.gr = gr
        return

    def addGAniso(self, ga):
        self.ga = ga
        return

    def __repr__(self):
        return "(" + str(self.pid) + "," + str(self.t) + ")"

    def __eq__(self, other):
        return self.pid == other.pid and self.t == other.t

    def __hash__(self):
        return self.pid + self.t


def toTLabel(t):
    return str(t) + "h"

def addPats(finRates, t):
    fin = open(finRates, 'r')
    crates = cPickle.load(fin)

    return [(cid, rate, findPattern1(cid, toTLabel(t)))
            for cid, rate in crates.iteritems()]

def addPats1(cs, t, tss):
    return [(cid, attr, findPattern1(cid, t, tss))
            for cid, attr in cs.items()]

def summRates(ratesPats, patId, f):
    return f([rate for cid, rate, pid in ratesPats if pid == patId and rate])

def gatherRates(ratesPats, patId):
    return [rate for cid, rate, pid in ratesPats if pid == patId and rate]


def mkPRates(ts):
    rateFs = {10: "../data/growthRates/10h_to_40h_growthrates.pkl",
              40: "../data/growthRates/40h_to_96h_growthrates.pkl",
              96 : "../data/growthRates/96h_to_120h_growthrates.pkl",
              120 : "../data/growthRates/120h_to_132h_growthrates.pkl"}

    return dict([(t, addPats(rateFs[t], t)) for t in ts])

def mkPRates1(tss):
    return dict([(t, addPats1(cs, t)) for t, cs in tss.iteritems()])

def mkPRatesRev(ts):
    rateFs = {40: "../data/growthRates/40h_to_10h_growthrates.pkl",
              96: "../data/growthRates/96h_to_40h_growthrates.pkl",
              120 : "../data/growthRates/120h_to_96h_growthrates.pkl",
              132 : "../data/growthRates/132h_to_120h_growthrates.pkl"}

    return dict([(t, addPats(rateFs[t], t)) for t in ts])

def addGrowthRates(G):
    prates = mkPRates([10, 40, 96, 120])

    for n in G.nodes():
        if not n.t == 132:
            n.addGrowthRate(summRates(prates[n.t], n.pid, np.mean))

    return

def addGrowthRates1(G, prates):
    for n in G.nodes():
        n.addGrowthRate(summRates(prates[n.t], n.pid, np.mean))

    return

def addGrowthRatesRev(G):
    prates = mkPRatesRev([40, 96, 120, 132])

    for n in G.nodes():
        if not n.t == 10:
            n.addGrowthRate(summRates(prates[n.t], n.pid, avgOr))

    return

def addGrowthRatesRevAvg(G):
    prates = mkPRates([10, 40, 96, 120])
    pratesRev = mkPRatesRev([40, 96, 120, 132])

    for n in G.nodes():
        if n.t == 10:
            n.addGrowthRate(summRates(prates[n.t], n.pid, np.mean))
        elif n.t == 132:
            n.addGrowthRate(summRates(pratesRev[n.t], n.pid, avgOr))
        else:
            gr = summRates(prates[n.t], n.pid, np.mean)
            grRev = summRates(pratesRev[n.t], n.pid, avgOr)
            n.addGrowthRate(np.mean([gr, grRev]))

    return

def addGAnisos(G, ganisos):
    for n in G.nodes():
        n.addGAniso(summRates(ganisos.get(n.t, []), n.pid, avgOr))

    return

def addGRates(G, grates):
    for n in G.nodes():
        n.addGrowthRate(summRates(grates.get(n.t, []), n.pid, avgOr))

    return

def addGAnisos1(G):
    ganisos = getGAnisos1()

    for n in G.nodes():
        if not n.t == 132:
            n.addGAniso(summRates(ganisos[n.t], n.pid, avgOr))

    return

def addGAnisosRevAvg(G):
    ganisos = getGAnisos1()
    ganisosRev = getGAnisosRev1()

    for n in G.nodes():
        if n.t == 10:
            n.addGAniso(summRates(ganisos[n.t], n.pid, np.mean))
        elif n.t == 132:
            n.addGAniso(summRates(ganisosRev[n.t], n.pid, avgOr))
        else:
            gan = summRates(ganisos[n.t], n.pid, np.mean)
            ganRev = summRates(ganisosRev[n.t], n.pid, avgOr)
            n.addGAniso(np.mean([gan, ganRev]))

    return

def getGAnisosPerPattern(G, ganisos):
    ganisosState = defaultdict(list)

    for n in G.nodes():
        ganisosState[n.pid] += gatherRates(ganisos.get(n.t, []), n.pid)

    return ganisosState

def getGAnisosPerPatternTime(G, ganisos):
    ganisosStateT = defaultdict(dict)

    for n in G.nodes():
        ganisosStateT[n.t][n.pid] = gatherRates(ganisos.get(n.t, []), n.pid)

    return ganisosStateT

def getGAnisosPerGene(tss, ganisos):
    ganisosGene = defaultdict(list)

    for t, gas in ganisos.items():
        for c, a in gas.items():
            if c in tss[t].cells:
                for g, expr in tss[t].cells[c].exprs.items():
                    if expr: ganisosGene[g].append(a)
                
    return ganisosGene

def getGAnisosPerGeneTime(tss, ganisos):
    ganisosGeneT = dict()
    for t in ganisos.keys():
        ganisosGeneT[t] = defaultdict(list)

    for t, gas in ganisos.items():
        for c, a in gas.items():
            if c in tss[t].cells:
                for g, expr in tss[t].cells[c].exprs.items():
                    if expr: ganisosGeneT[t][g].append(a)
                
    return ganisosGeneT

def addGAnisosRevAvgData(G):
    ganisos = getGAnisos()
    ganisosRev = getGAnisosRevData()

    for n in G.nodes():
        if n.t == 10:
            n.addGAniso(summRates(ganisos[n.t], n.pid, np.mean))
        elif n.t == 132:
            n.addGAniso(summRates(ganisosRev[n.t], n.pid, avgOr))
        else:
            gan = summRates(ganisos[n.t], n.pid, np.mean)
            ganRev = summRates(ganisosRev[n.t], n.pid, avgOr)
            n.addGAniso(np.mean([gan, ganRev]))

    return


#blue:1
#red:2
#dot:3
def mkTGraphN():
    edges = [(TPat(1, 10), TPat(5, 40), 1),
             (TPat(2, 10), TPat(7, 40), 1),
             (TPat(3, 10), TPat(7, 40), 1),
             (TPat(4, 10), TPat(4, 40), 1),
             (TPat(31, 10), TPat(5, 40), 2),
             (TPat(2, 10), TPat(5, 40), 2),
             (TPat(4, 40), TPat(12, 96), 1),
             (TPat(7, 40), TPat(12, 96), 3),
             (TPat(7, 40), TPat(8, 96), 3),
             (TPat(7, 40), TPat(9, 96), 1),
             (TPat(7, 40), TPat(11, 96), 3),
             (TPat(7, 40), TPat(10, 96), 1),
             (TPat(5, 40), TPat(9, 96), 2),
             (TPat(11, 96), TPat(17, 120), 1),
             (TPat(9, 96), TPat(17, 120), 1),
             (TPat(9, 96), TPat(16, 120), 1),
             (TPat(8, 96), TPat(16, 120), 1),
             (TPat(9, 96), TPat(20, 120), 3),
             (TPat(8, 96), TPat(18, 120), 1),
             (TPat(12, 96), TPat(18, 120), 3),
             (TPat(12, 96), TPat(20, 120), 3),
             (TPat(12, 96), TPat(19, 120), 1),
             (TPat(10, 96), TPat(20, 120), 3),
             (TPat(10, 96), TPat(19, 120), 1),
             (TPat(10, 96), TPat(14, 120), 3),
             (TPat(10, 96), TPat(15, 120), 1),
             (TPat(10, 96), TPat(13, 120), 1),
             (TPat(8, 96), TPat(17, 120), 2),
             (TPat(13, 120), TPat(22, 132), 1),
             (TPat(13, 120), TPat(24, 132), 1),
             (TPat(15, 120), TPat(24, 132), 1),
             (TPat(15, 120), TPat(23, 132), 1),
             (TPat(14, 120), TPat(14, 132), 1),
             (TPat(16, 120), TPat(16, 132), 3),
             (TPat(16, 120), TPat(26, 132), 3),
             (TPat(16, 120), TPat(27, 132), 1),
             (TPat(17, 120), TPat(27, 132), 1),
             (TPat(17, 120), TPat(18, 132), 1),
             (TPat(18, 120), TPat(18, 132), 1),
             (TPat(18, 120), TPat(26, 132), 3),
             (TPat(19, 120), TPat(25, 132), 1),
             (TPat(19, 120), TPat(29, 132), 1),
             (TPat(20, 120), TPat(30, 132), 1)]
        
    G = nx.DiGraph()
    for n1, n2, connTyp in edges:
        G.add_edge(n1, n2, connTyp=connTyp)

    return G

def nodePoss(G):
    poss = [((1, 10), (0, -10)),
            ((2, 10), (0, -20)),
            ((3, 10), (0, -30)),
            ((4, 10), (0, 0)),
            ((32, 10), (0, -5)),
            ((4, 40), (20, 5)),
            ((5, 40), (20, -5)),
            ((7, 40), (20, -18)),
            ((8, 96), (40, -25)),
            ((9, 96), (40, -5)),
            ((10, 96), (40, -35)),
            ((11, 96), (40, -15)),
            ((12, 96), (40, 5)),
            ((13, 120), (60, -35)),
            ((14, 120), (60, -55)),
            ((15, 120), (60, -45)),
            ((16, 120), (60, -15)),
            ((17, 120), (60, 5)),
            ((18, 120), (60, -5)),
            ((19, 120), (60, -25)),
            ((20, 120), (60, 15)),
            ((16, 132), (80, -15)),
            ((29, 132), (80, -25)),
            ((18, 132), (80, 15)),
            ((22, 132), (80, -45)),
            ((23, 132), (80, -65)),
            ((24, 132), (80, -55)),
            ((25, 132), (80, -35)),
            ((26, 132), (80, -5)),
            ((27, 132), (80, 5)),
            ((30, 132), (80, 25)),
            ((14, 132), (80, -75)),
            ((31, 10), (0, 10))]

    possD = dict([(tp, np.array(pos)) for tp, pos in poss])

    return dict([(g, possD[(g.pid, g.t)]) for g in G.nodes()])


def nodePoss1(G):
    poss = [((1, 10), (0, -20)),
            ((2, 10), (0, -40)),
            ((3, 10), (0, -60)),
            ((4, 10), (0, 20)),
            ((31, 10), (0, 0 )),
            ((4, 40), (20, 10)),
            ((5, 40), (20, -15)),
            ((7, 40), (20, -36)),
            ((8, 96), (40, -45)),
            ((9, 96), (40, 0)),
            ((10, 96), (40, -70)),
            ((11, 96), (40, -25)),
            ((12, 96), (40, 30)),
            ((13, 120), (60, -60)),
            ((14, 120), (60, -105)),
            ((15, 120), (60, -85)),
            ((16, 120), (60, -15)),
            ((17, 120), (60, 30)),
            ((18, 120), (60, 5)),
            ((19, 120), (60, -35)),
            ((20, 120), (60, 50)),
            ((14, 132), (80, -160)),
            ((16, 132), (80, -25)),
            ((18, 132), (80, 45)),
            ((22, 132), (80, -55)),
            ((23, 132), (80, -130)),
            ((24, 132), (80, -110)),
            ((25, 132), (80, -100)),
            ((26, 132), (80, 0)),
            ((27, 132), (80, 20 )),
            ((29, 132), (80, -75)),
            ((30, 132), (80, 80))]


    possD = dict([(tp, np.array(pos)) for tp, pos in poss])

    return dict([(g, possD[(g.pid, g.t)]) for g in G.nodes()])

def avgOr(ns, df=0.0):
    if not ns: return df

    return np.mean(ns)

def avg2(v1, v2):
    return np.mean([v1, v2])

def ganiso(n):
    return n.ga

def gr(n):
    return n.gr

def drawTGraph(G, attr):
    pos = nodePoss(G)
    grates = [attr(g) for g in G.nodes()]
    ws = nx.get_edge_attributes(G, 'connTyp')
    nodes = nx.draw_networkx_nodes(G, pos, node_color=grates,
                                   cmap=plt.cm.Blues)#, vmin=0.0, vmax=0.3)

    labels = {n:str(n.pid) for n in G.nodes()}
    nx.draw_networkx_labels(G, pos, labels, font_size=10,
                            font_family='sans-serif')#, font_color='slategrey')

    blueEdges = [e for e in G.edges() if ws[e] == 1]
    redEdges = [e for e in G.edges() if ws[e] == 2]
    dotEdges = [e for e in G.edges() if ws[e] == 3]

    print(len(blueEdges))
    print(len(redEdges))
    print(len(dotEdges))
    
    nx.draw_networkx_edges(G, pos, edgelist=blueEdges, edge_color='b')
    nx.draw_networkx_edges(G, pos, edgelist=redEdges, edge_color='r')
    nx.draw_networkx_edges(G, pos, edgelist=dotEdges, style='dashed')

    plt.colorbar(nodes, shrink=0.5)#, ticks=[0.0, 0.15, 0.3])
    plt.axis('off')
    
    plt.savefig("transitionGrowth.svg", dpi=300, box_inches='tight')
