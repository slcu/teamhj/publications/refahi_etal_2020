## Introduction
This directory contains code and data needed to recreate all the 
growth and growth anisotropy analysis figures in the article.


## Scripts and Methods
This part of the analysis code uses Python 3 (any version < 3.9 should work).
To setup a local Python virtual environment:
```
python3 -m venv atlas-env
source atlas-env/bin/activate
```

Install requirements inside this environment and generate textual representations of tissue geometries and rel. information:
```
pip install --upgrade pip
pip install -r ../requirements.txt
pip install notebook
pip install ../common/
chmod a+x to_fm.sh
./to_fm.sh
```

To run things:
```
# run specific for flower 1
python -c 'import growth_control; growth_control.go()'
jupyter nbconvert --to notebook --execute grates.ipynb
jupyter nbconvert --to notebook --execute anisos.ipynb

# run for all flowers
for i in {1..5}; do ./run-grate-distrs.sh i; done
for i in {1..5}; do ./run-aniso-distrs.sh i; done

```
or use the `run.sh` script that contains all the above instructions.

## Main Contacts
Argyris Zardilis argyris.zardilis@slcu.cam.ac.uk
