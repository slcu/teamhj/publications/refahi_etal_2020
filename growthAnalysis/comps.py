import common.lin as lin
import grates as grs
import patternTransitions as p
import growth_control as gc
import numpy as np
from scipy.stats import ttest_ind


def gratio(xs, xs1):
     if not xs or not xs1:
         return 0.0

     m1 = np.median(xs)
     m2 = np.median(xs1)

     return (m1 - m2) / (m1 + m2)

 
#load FM1 at specified timepoints
tss, linss = lin.mkSeries1(d="../data/FM1/tv/",
                           dExprs="../data/geneExpression/",
                           linDataLoc="../data/FM1/tracking_data/",
                           ft=lambda t: t in {10, 40, 96, 120, 132})
lin.filterL1_st(tss)
grates = grs.grates_avg_cons(tss, linss)
grates_gene = p.getGAnisosPerGene(tss, grates)
grates_st = gc.get_grs_state()


rgd_g = {}
for g in set(grates_gene.keys()).difference(set(['AHP6'])):
    grs1 = grates_gene['AHP6']
    grs2 = grates_gene[g]
    _, pval = ttest_ind(grs1, grs2)
    rgd_g[g] = (gratio(grs1, grs2),
                pval)

rgd_g_ = {}
for g in set(grates_gene.keys()).difference(set(['CUC1_2_3'])):
    grs1 = grates_gene['CUC1_2_3']
    grs2 = grates_gene[g]
    _, pval = ttest_ind(grs1, grs2)
    rgd_g_[g] = (gratio(grs1, grs2),
                 pval)

rgd_st26 = {}
for g in set(grates_st.keys()).difference(set([26, 27])):
    grs1 = grates_st[26] + grates_st[27]
    grs2 = grates_st[g]
    _, pval = ttest_ind(grs1, grs2)
    rgd_st26[g] = (gratio(grs1, grs2),
                   pval)

rgd_st16 = {}
for g in set(grates_st.keys()).difference(set([16, 17])):
    grs1 = grates_st[16] + grates_st[17]
    grs2 = grates_st[g]
    _, pval = ttest_ind(grs1, grs2)
    rgd_st16[g] = (gratio(grs1, grs2),
                   pval)


t=132
ts = tss[t]
g = gc.GPairRegions(t, ts, 'ETTIN', 'STM')
g1 = gc.GPairRegions(t, ts, 'AP1', 'LFY')

g.plot_distr()
g1.plot_distr()
