parallel jupyter nbconvert --ClearOutputPreprocessor.enabled=True --inplace {} ::: *.ipynb
rm *.png
rm *.svg
rm *.nbconvert.*