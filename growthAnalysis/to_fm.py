import common.lin as lin

tss, linss = lin.mkSeriesIm0(dataDir="../data/wt",
                             ft=lambda t: t in {10, 40, 96, 120, 132})

for t in tss.keys():
    with open("../data/wt/FM1/tv/{t}h_segmented_tvformat_volume_position.txt".format(t=str(t)), "w") as fout:
        print(t)
        fout.write(tss[t].toTVGeom())
